import UIKit
import FirebaseCore
//import YandexMobileMetricaPush
//import FBSDKCoreKit
//import AppsFlyerLib
//import UserNotifications
//import AdSupport

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  //Public variable
  public var window: UIWindow?
  public var orientationLock = UIInterfaceOrientationMask.portrait
  
  //Private variable
  private let rootVC            = RootVC()
//  private let appsFlyerGetData  = GDAppsFlyerGetData()
//  private let setupAppsFlyer    = GDSetupAppsfalyer()
//  private let setupFacebook     = GDSetupFacebook()
//  private var setupAppMetrica   = GDSetupAppMetrica()
  private let setupFirebase     = GDSetupFirebase()
//  private let setupNotification = GDSetupNotification()
  
  public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    //MARK: - Setup AppMetrica SDK.
  //  setupAppMetrica.activate()
    //MARK: - Setup Nitification
   // setupNotification.setup(application: application)
    //MARK: - Setup Firebase
    setupFirebase.setup()
    //MARK: - Setup AppsFlyer
    //setupAppsFlyer.setup()
    //MARK: - Setup Facebook
    //setupFacebook.setup()
   // _ = setupFacebook.setupDidFinishOptionsMetod(application, launchOptions)
    //MARK: - Set root view controller
    self.rootVC.set(window: window)
    return true
  }
//  func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//    // Reports app open from deep link for iOS 10 or later
//    AppsFlyerLib.shared().handleOpen(url, options: options)
//    return true
//  }
//  func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//    return true
//  }
//  // Reports app open from a Universal Link for iOS 9 or later
//  func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
//    AppsFlyerLib.shared().continue(userActivity, restorationHandler: restorationHandler)
//    return true
//  }
//  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//
//  }
//  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
//    //appmetrica post deviceToken
//    self.setupAppMetrica.setDeviceToken(deviceToken: deviceToken)
//    //AppsFlyer notification remove
//    AppsFlyerLib.shared().registerUninstall(deviceToken)
//  }
//  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//    //setup YandexMetrica
//    self.setupAppMetrica.handlePushNotification(userInfo)
//    //setup AppsFlyer
//    self.appsFlyerGetData.onConversionDataSuccess(userInfo)
//    self.appsFlyerGetData.onAppOpenAttribution(userInfo)
//    //Print full message.
//    print(userInfo, "didReceiveRemoteNotification AppDelegate")
//  }
//  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//    //setup YandexMetrica
//    self.setupAppMetrica.handlePushNotification(userInfo)
//    //setup AppsFlyer
//    self.appsFlyerGetData.onConversionDataSuccess(userInfo)
//    print(userInfo, "didReceiveRemoteNotification fetchCompletionHandler AppDelegate")
//    completionHandler(.newData)
//  }
//  func applicationDidBecomeActive(_ application: UIApplication) {
//    AppEvents.activateApp()
//  }
//  func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//    return self.orientationLock
//  }
}


