
import UIKit

public class ProfileTopView: UIView {
  
  public override func draw(_ rect: CGRect) {
    self.drawCanvas1(frame: rect, resizing: .aspectFill)
  }
  
  private func drawCanvas1(frame targetFrame: CGRect = CGRect(x: 0, y: 0, width: 595, height: 467), resizing: ResizingBehavior = .aspectFit) {
    //// General Declarations
    let context = UIGraphicsGetCurrentContext()!
    
    //// Resize to Target Frame
    context.saveGState()
    let resizedFrame: CGRect = resizing.apply(rect: CGRect(x: 0, y: 0, width: 595, height: 467), target: targetFrame)
    context.translateBy(x: resizedFrame.minX, y: resizedFrame.minY)
    context.scaleBy(x: resizedFrame.width / 595, y: resizedFrame.height / 467)
    
    
    //// Color Declarations
    let gradientColor = UIColor.set(.oneGradient)
    let gradientColor2 = UIColor.set(.twoGradient)
    
    //// Gradient Declarations
    let gradient = CGGradient(colorsSpace: nil, colors: [gradientColor.cgColor, gradientColor2.cgColor] as CFArray, locations: [0, 1])!
    
    //// Bezier Drawing
    let bezierPath = UIBezierPath()
    bezierPath.move(to: CGPoint(x: 0, y: -0))
    bezierPath.addLine(to: CGPoint(x: 0, y: 394.12))
    bezierPath.addLine(to: CGPoint(x: 55.23, y: 386.98))
    bezierPath.addCurve(to: CGPoint(x: 250.88, y: 397.43), controlPoint1: CGPoint(x: 120.57, y: 378.53), controlPoint2: CGPoint(x: 187.25, y: 382.09))
    bezierPath.addLine(to: CGPoint(x: 290.96, y: 407.09))
    bezierPath.addCurve(to: CGPoint(x: 547.42, y: 406.07), controlPoint1: CGPoint(x: 374.89, y: 427.31), controlPoint2: CGPoint(x: 463.71, y: 426.96))
    bezierPath.addLine(to: CGPoint(x: 595.27, y: 394.12))
    bezierPath.addLine(to: CGPoint(x: 595.27, y: -0))
    bezierPath.addLine(to: CGPoint(x: 0, y: -0))
    bezierPath.close()
    context.saveGState()
    bezierPath.addClip()
    context.drawLinearGradient(gradient, start: CGPoint(x: 457.38, y: 473.74), end: CGPoint(x: 137.9, y: -79.62), options: [])
    context.restoreGState()
    
    context.restoreGState()
    
  }
  
  
  
  
  @objc(ProfileTopViewResizingBehavior)
  public enum ResizingBehavior: Int {
    case aspectFit /// The content is proportionally resized to fit into the target rectangle.
    case aspectFill /// The content is proportionally resized to completely fill the target rectangle.
    case stretch /// The content is stretched to match the entire target rectangle.
    case center /// The content is centered in the target rectangle, but it is NOT resized.
    
    public func apply(rect: CGRect, target: CGRect) -> CGRect {
      if rect == target || target == CGRect.zero {
        return rect
      }
      
      var scales = CGSize.zero
      scales.width = abs(target.width / rect.width)
      scales.height = abs(target.height / rect.height)
      
      switch self {
        case .aspectFit:
          scales.width = min(scales.width, scales.height)
          scales.height = scales.width
        case .aspectFill:
          scales.width = max(scales.width, scales.height)
          scales.height = scales.width
        case .stretch:
          break
        case .center:
          scales.width = 1
          scales.height = 1
      }
      
      var result = rect.standardized
      result.size.width *= scales.width
      result.size.height *= scales.height
      result.origin.x = target.minX + (target.width - result.width) / 2
      result.origin.y = target.minY + (target.height - result.height) / 2
      return result
    }
  }
}
