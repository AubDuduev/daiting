
import Foundation

struct GDUpdateUser {
  
  let userAnket: CODAnket!
  let anket    : CODAnket!
}

class GDMessages {
  
  let fromUserData: CODUserData! 
  let toUserData  : CODAnket!
  
  init(fromUserData: CODUserData, toUserData: CODAnket) {
    self.fromUserData = fromUserData
    self.toUserData   = toUserData
  }
}

class GDGetMessages {
  
  let fromUserData: CODUserData!
  let toUserData  : CODAnket!
  
  init(fromUserData: CODUserData, toUserData: CODAnket) {
    self.fromUserData = fromUserData
    self.toUserData   = toUserData
  }
}

struct GDSendMessages {
  
  let fromUserData: CODUserData!
  let toUserData  : CODAnket!
  let message     : CODMessage!
  
  init(fromUserData: CODUserData, toUserData: CODAnket, message: CODMessage) {
    self.fromUserData = fromUserData
    self.toUserData   = toUserData
    self.message      = message
  }
}
struct GDMutually {
  
  let ID          : String!
  var email       : String!
  var name        : String!
  var city        : String!
  var age         : String!
  var gender      : String!
  var searchGender: String!
  var searchCity  : String!
  var searchAge   : String!
  var avatar      : String!
  var isSympathy  : Bool!
  var isDelight   : Bool!
  var blackList   : Bool!
  
  init(anket: CODAnket) {
    
    self.ID           = anket.ID
    self.email        = anket.email
    self.avatar       = anket.avatar
    self.name         = anket.name
    self.city         = anket.city
    self.age          = anket.age
    self.gender       = anket.gender
    self.searchGender = anket.searchGender
    self.searchCity   = anket.searchCity
    self.searchAge    = anket.searchAge
    self.isSympathy   = anket.isLike
    self.isDelight    = anket.isDelight
    self.blackList    = anket.blackList
  }
}

class GDInvitation {
  
  let selfUser  : CODUserData!
  let toUserData: CODAnket!
  
  init(selfUser: CODUserData, toUserData: CODAnket) {
    self.selfUser   = selfUser
    self.toUserData = toUserData
  }
}
