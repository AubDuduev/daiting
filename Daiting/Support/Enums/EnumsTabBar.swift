//
//  TabBarImages.swift
//  UmmaSPB
//
//  Created by Aleksandr on 16.02.2020.
//  Copyright © 2020 Aleksandr. All rights reserved.
//
import Foundation

enum TabBarImage: String, CaseIterable {
  
  case chats
  case likes
  case pair
  case user
  case nikah
}

enum TabBarText: String, CaseIterable {
  
  case chats   = "Chats"
  case likes   = "Likes"
  case searh   = "Pair"
  case me      = "User"
  case support = "Nikah"
	
	private func localizedString() -> String {
			return NSLocalizedString(self.rawValue, comment: "")
	}

	static func getTextFor(title: TabBarText) -> String {
			return title.localizedString()
	}
}
