
import Foundation

protocol AlertButtonOptionsoble {
  
  var buttonsCount: Int { get }
  var buttonsText : Array<String> { get set }
}
