//
//  Extension + UIResponder.swift
import UIKit

extension UIResponder {
	
	static var identifier: String {
		get {
			return String(describing:self)
		}
	}
	//MARK: - Set localozed text
	func localizedText(_ text: Texts.Project){
		if let label = self as? UILabel {
			label.text = NSLocalizedString(text.getTextFor(text), comment: "")
		} else if let button = self as? UIButton {
			button.setTitle(NSLocalizedString(text.getTextFor(text), comment: ""), for: .normal)
		}
	}
}
