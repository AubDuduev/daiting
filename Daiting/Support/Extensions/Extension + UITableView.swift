//
//  Extension + UITableView.swift


import UIKit

extension UITableView {
  
  func scrollTo(_ to: ScrollPosition){
    switch to {
      case .bottom:
        let numberOfSections = self.numberOfSections
        let numberOfRows     = self.numberOfRows(inSection: numberOfSections - 1)
        if numberOfRows > 0 {
          let indexPath = IndexPath(row: numberOfRows - 1, section: (numberOfSections - 1))
          self.scrollToRow(at: indexPath, at: to, animated: true)
        }
      case .top:
        let indexPath = IndexPath(row: 0, section: 0)
        self.scrollToRow(at: indexPath, at: to, animated: true)
      default:
        break
    }
  }
}
