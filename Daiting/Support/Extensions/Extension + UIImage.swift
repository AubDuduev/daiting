//
//  Extension + UIImage.swift

import UIKit
extension UIImage {
	
	func resize(withPercentage percentage: CGFloat) -> UIImage? {
		let newRect = CGRect(origin: .zero, size: CGSize(width: size.width*percentage, height: size.height*percentage))
		UIGraphicsBeginImageContextWithOptions(newRect.size, true, 1)
		self.draw(in: newRect)
		defer {UIGraphicsEndImageContext()}
		return UIGraphicsGetImageFromCurrentImageContext()
	}
	
	func resizeTo(MB: Double) -> UIImage? {
		guard let fileSize = self.pngData()?.count else {return nil}
		let fileSizeInMB = CGFloat(fileSize)/(1024.0*1024.0)//form bytes to MB
		let percentage = 1/fileSizeInMB
		return resize(withPercentage: percentage)
	}
  
  static func set(_ nameImage: UIImageView.NameImage) -> UIImage {
   
    if let image = UIImage(named: nameImage.rawValue) {
      return image
    }
    return UIImage()
  }
  
}

