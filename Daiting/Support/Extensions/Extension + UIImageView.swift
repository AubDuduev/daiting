//
//  Extension + UIImageView.swift

import UIKit

extension UIImageView {
	
	func set(nameImage: NameImage){
		self.image = UIImage(named: nameImage.rawValue)
	}
	func set(name: TabBarImage){
    self.image = UIImage(named: name.rawValue)
  }
	enum NameImage: String {
		
    case bubbleMessageLeft
    case messageBubleRight
    case messageBubleLeft
		case userTextField
		case passwordTextField
		case passwordConfirmTextField
		case lastNameTextField
		case firstNameTextField
    case male
    case female
    case freeWending
    case wedding
    case freeWedding
	}
}



