//
//  TextsProject.swift
//  DgBetTrip
//
//  Created by Senior Developer on 19.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import UIKit

class Texts {
  static func text(_ text: Project) -> String {
    return text.rawValue
  }
  enum Title: String, CaseIterable {
    
    case empty        = ""
    case error        = "ОШИБКА"
    case noNetwork    = "НЕТ ИНТЕРНЕТА"
    case noMessage    = "НЕТ СООБЩЕНИЙ"
    case sendEmail    = "ОТПРАВЛЕН"
    case save         = "Сохранен"
    case registration = "Регистрация"
    case information  = "Информация"
    case install      = "Установить"
    case addPhoto     = "Изменить фото"
    
    private func localizedString() -> String {
      return NSLocalizedString(self.rawValue, comment: "")
    }
    
    func getTextFor(title: Title?) -> String {
      return title!.localizedString()
    }
  }
  enum Message: String, CaseIterable {
    
    case empty              = ""
    case noNetwork          = "Отсутствует подключение к сети, подключите Ваше устройство и попробуйте снова"
    case noJSON             = "Извините произошла ошибка получения данных"
    case noMessage          = "У Вас нет не одного сообщения"
    case emptyField         = "Введите корректные данные и попробуйте снова"
    case sendEmail          = "Ваш email отправлен"
    case tryEnded           = "Количество попыток закончилось"
    case registrRequaer     = "Для этого действия требуется регистрация"
    case error              = "Произошла ошибка попробуйте позже"
    case telegramm          = "Для участия в конкурсе необходимо подписаться на телеграм бота, Вам необходимо иметь учетную запись Телеграм"
    case addPhotoChange     = "Фото взять из библиотеки или сделать фотографию?"
    case passworNotConfirm  = "Пароли не соподают"
    case succesRegistration = "Регистрация завершена, спасибо , что воспользовались нашим сервисом"
    case lockChat           = "Вы можете заблокировать чат и Вам не будут приходить сообщения от службы поддержки, и Вы в любой момент можете разблокировать связь с нами"
    case isLockChat         = "Вы заблокировали переписку со службой поддержки и теперь Вы не можете не получать не отправлять сообщения, разблокируйте чат нажав 'Замочек' в верхнем правом углу"
    case errorUnknown       = "Неизвестная ошибка"
    case blackListEmpty     = "Черный список пустой"
    case messagesEmpty      = "Сообщений нет"
    case simphatyEmpty      = "Симпатий нет"
    case noMailCompose      = "Ошибка"
    case exitProfile        = "Вы деиствительно хотите Выйти?"
    
    private func localizedString() -> String {
      return NSLocalizedString(self.rawValue, comment: "")
    }
    
    func getTextFor(message: Message?) -> String {
      return message!.localizedString()
    }
  }
  enum MessageCustom {
    case message(String)
  }
  enum Worning {
    case non
  }
  
  enum Project: String {
    
    case empty
    case noSuccesChat                 = "Чат доступен по подписке. Узнать подробнее."
    case offerText                    = "Нажимая на кнопку, Вы соглашаетесь с условиями публичной оферты, действующими тарифами сервиса, даете свое согласие на обработку персональных данных и на получение рекламных материалов, осознаете возмездный характер оказываемых услуг"
    case chatTelegramViewText         = #"Смотри, в боте и приложении каждый день выходит бесплатно 1 прогноз, ты можешь начать поднимать деньги уже сегодня, при этом без вложений! Но я рекомендую тебе купить подписку на 7 дней для старта за 3500р. Там каждый день выходит 5 серий, что позволяет зарабатывать в 5 раз больше! Также у меня сейчас проходит розыгрыш на бесплатный доступ к подписке "Серия Побед" на 1 месяц. Для участия регистрируйся в БК, ссылка придет ниже. Присылай мне скриншот из личного кабинета букмекерки и сегодня до конца дня мы выберем рандомно победителя и откроем доступ бесплатно, эта подписка золотая жила, минимум ребята зарабатывают от 150000р в месяц. Кстати бонусом от меня по промокоду «code» будет до 30000р на счет."#
    case servicesTitleButtonsActive   = "Приобретай подписку прямо сейчас и начинай зарабатывать!"
    case servicesTitleButtonsNoActive = "Приобретай подписку прямо сейчас и начинай зарабатывать \n от 100.000 рублей в месяц"
    case servicesChanelActive         = "”Серия Побед” - моя личная разработка 5-6 серий в день с высокой проходимостью. \n Стратегия отлично подходит всем, кто хочет разогнать свой счет и иметь ежедневный доход. Я каждому индивидуально помогу рассчитать бюджет так, чтобы у вас не возникало дополнительных вопросов!"
    case servicesSerialVictoryMode0   = "“Серия Побед“ - моя личная разработка 5-6 серий в день! Доходность 9-12% от банка в сутки. Если Ваш банк составляет 10000 р. то за неделю вы заработаете 17-20 тысяч р. Стратегия отлично подходит всем, кто хочет разогнать свой счет! После покупки нашей услуги Вы получите доступ к закрытому каналу “Серия Побед“, а также персональное обучение от Diego Garcia посредством аудио и видеосообщений в персональном чате со мной."
    case servicesSerialVictoryMode1   = "“Серия побед” - моя личная разработка 5-6 серий в день! Доходность 9-12% от банка в сутки. Если ваш банк составляет 10 000 ₽, то за неделю вы заработаете 17-20 тысяч ₽. Стратегия отлично подходит всем, кто хочет разогнать свой счет!"
    case promotionNavBarTitle         = "Спецпредложение"
    case promotionMessageTitle        = "Тариф “Триал“ всего за 250₽*"
    case promotionMessage             = "1️⃣ Обучение ставкам с 0 и до уровня профи ☑️ \n  \n 2️⃣ Помогаем распланировать бюджет, чтобы всегда быть в плюсе ☑️ \n  \n 3️⃣ Проведем за руку до 100.000₽, даже если у вас есть только минимальный банк на старте ☑️ \n  \n 4️⃣ Предоставляем связь менеджера 24/7 , решим любые вопросы, касающихся ставок, оказываем поддержку ☑️ \n  \n 5️⃣ Каждый день даем уверенные прогнозы, ты получаешь бабло, отбиваешь подписку за один день и выходишь в плюс ☑️"
    case promotionDisconte            = "250₽*"
    case promotionPrice               = "1000₽"
    case promotionPriceDay            = "/ нед."
    
    private func localizedString() -> String {
      return NSLocalizedString(self.rawValue, comment: "")
    }
    func getTextFor(_ text: Project?) -> String {
      return text!.localizedString()
    }
  }
}
enum TextType {
  
  case Title
  case Message
  case Project
}
enum TextAlert: String, CaseIterable {
  
  case empty
  case noJSON
  case noNetwork
  case noSendedEmail
  case yesSendedEmail
  case inputEmail
  case emptyField
  case noMessage
  case sendEmail
  case tryEnded
  case saveLike
  case registrRequaer
  case changeTour
  
}
enum TextSimple {
  case some
}



