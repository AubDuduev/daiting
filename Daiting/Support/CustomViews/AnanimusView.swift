
import UIKit
import SwiftEntryKit

class AnanimusView: UIView {

  @IBOutlet weak var toAnketImageView : UIImageView!
  @IBOutlet weak var fromAnkeImageView: UIImageView!
  @IBOutlet weak var commonView       : UIView!
  @IBOutlet weak var toNameLabel      : UILabel!
  @IBOutlet weak var fromNameLabel    : UILabel!
  
  public var aceptClousure: ClousureEmpty!
  public var currentIndex = 0
  public var postAnket    : CODAnket!
  
  private let serverFB = ServerFB()
  
  public func updateData(index: Int){
    self.currentIndex = index
  }
  private func avatar(){
    let corner: CGFloat = 135 / 2
    toAnketImageView.cornerRadius(corner, true)
    fromAnkeImageView.cornerRadius(corner, true)
    fromAnkeImageView.borderColor(.red, 2)
    toAnketImageView.borderColor(.red, 2)
    self.commonView.cornerRadius(5, true)
    self.image()
    self.data()
  }
  private func data(){
    toNameLabel.text = GVuserData?.name
  }
  public func deleteInvantion(){
    guard let userData = GVuserData else { return }
    //Request
    self.serverFB.request(requestType: .DELETEInvantion, data: userData) { (serverResult) in}
  }
  private func image(){
    guard let image = GVuserData?.avatar else { return }
    guard let url   = URL(string: image) else { return }
    toAnketImageView.isSkeletonable = true
    toAnketImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .white))
    toAnketImageView.sd_setImage(with: url) { [weak self] (_, error, _, url) in
      guard let self = self else { return }
      if let error = error {
        print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
      } else {
        self.toAnketImageView.hideSkeleton()
      }
    }
  }
  @IBAction func aceptButton(button: UIButton){
    SwiftEntryKit.dismiss(){ [weak self] in
      guard let self = self else { return }
      self.aceptClousure?()
      self.deleteInvantion()
    }
  }
  @IBAction func delayButton(button: UIButton){
    SwiftEntryKit.dismiss(){
      self.deleteInvantion()
    }
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.avatar()
  }
}
