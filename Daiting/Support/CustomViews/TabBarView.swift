import AnimatedGradientView
import UIKit
import SnapKit

class TabBarView: UIView {
  
  public var actions: Clousure<Int>!
  
  private var isAddedGradien = false
  
  //MARK: - Outlets
  @IBOutlet weak var commonStackView: UIStackView!
  
  //MARK: - Array outlets
  @IBOutlet var iconsImageViews  : [UIImageView]!
  @IBOutlet var iconsViews       : [UIView]!
  @IBOutlet var titleLabels      : [UILabel]!
  @IBOutlet var buttonLabels     : [UIButton]!
  @IBOutlet var sectionStackViews: [UIStackView]!
  
  //MARK: - Outlets Constraint
  @IBOutlet weak var topCommonStackViewConstant: NSLayoutConstraint!
  @IBOutlet var heightLabelsConstant           : [NSLayoutConstraint]!
  
  public func setup(){
    //Gradient
    self.tabBarViewGradient()
    //Stack view
    self.topCommonStackViewConstant.constant = 10
    self.sectionStackViews.forEach { $0.spacing = 6 }
    //Labels
    self.titleLabels.forEach { $0.text = TabBarText.allCases[$0.tag].rawValue }
    self.titleLabels.forEach { $0.font = .set(.lemon, 15) }
    self.titleLabels.forEach { $0.textColor = .white }
    self.heightLabelsConstant.forEach {$0.constant = 20 }
    //Images
    self.iconsImageViews.forEach { $0.tintColor = .white }
    self.iconsImageViews.forEach { $0.image     = UIImage(named: TabBarImage.allCases[$0.tag].rawValue )}
    //Backgraund
    self.iconsImageViews.forEach { $0.backgroundColor = .clear }
    self.titleLabels.forEach { $0.backgroundColor     = .clear }
    self.iconsViews.forEach  { $0.backgroundColor     = .clear }
    self.backgroundColor = .black
    //Frame
    self.cornerRadius(15, false)
    self.shadowColor(color: .black, radius: 5)
  }
  public func tabBarViewGradient(){
    let animatedGradient = AnimatedGradientView()
    animatedGradient.direction = .upLeft
    animatedGradient.colors = [[.set(.oneGradient), .set(.twoGradient)]]
    animatedGradient.removeFromSuperview()
    animatedGradient.cornerRadius(10, true)
    self.insertSubview(animatedGradient, at: 0)
    animatedGradient.snp.makeConstraints { (gradient) in
      gradient.edges.equalTo(self)
    }
  }
  private func animationClick(tag: Int){
    UIView.animate(withDuration: 1, delay: 0,
                   usingSpringWithDamping: 0.5,
                   initialSpringVelocity: 1.0,
                   options: .curveEaseInOut, animations: { [weak self] in
      guard let self = self else { return }
      //Images set color
      self.iconsImageViews.forEach{ $0.tintColor = .white }
      self.iconsImageViews[tag].tintColor        = .black
      //Label set color
      self.titleLabels.forEach{ $0.textColor     = .white }
      self.titleLabels[tag].textColor            = .black
    })
  }
  
  @IBAction func actionButton(button: UIButton){
    self.actions?(button.tag)
    self.animationClick(tag: button.tag)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}


