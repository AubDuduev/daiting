
import UIKit
import AnimatedGradientView

class NavBarView: UIView {
  
  @IBOutlet weak var titleLabel: UILabel!
  
  public func setup(){
    self.backgroundColor = .set(.navBarFon)
    self.navBarViewGradient()
  }
  public func navBarViewGradient(){
    let animatedGradient = AnimatedGradientView(frame: self.frame)
    animatedGradient.direction = .upLeft
    animatedGradient.colors = [[.set(.oneGradient), .set(.twoGradient)]]
    if !self.isDescendant(of: animatedGradient) {
      self.insertSubview(animatedGradient, at: 0)
    }
  }
  override init(frame: CGRect) {
    super.init(frame: .zero)
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}
