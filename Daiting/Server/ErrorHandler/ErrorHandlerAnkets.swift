
import UIKit

class ErrorHandlerAnkets {
   
  public func check(ankets: [CODAnket]?) -> Bool {
    do {
      try self.error(ankets: ankets)
    } catch HandlerError.Succes {
      return true
    } catch HandlerError.Nil {
      AlertEK.customText(title: .error, message: .message(HandlerError.Nil.rawValue))
      return false
    } catch HandlerError.Empty {
      //AlertEK.customText(title: .information, message: .message(HandlerError.Empty.rawValue))
      return false
    } catch {
      AlertEK.dеfault(title: .error, message: .errorUnknown)
      return false
    }
  return false
  }
  private func error(ankets: [CODAnket]?) throws  {
    
    //Ошибка получения
    guard let ankets = ankets else { throw HandlerError.Nil }
    
    //Сообщения получены , но их нет
    if ankets.isEmpty {
      throw HandlerError.Empty
    }
    
    //Ошибки не обнаружены
    throw HandlerError.Succes
  }
  
  private enum HandlerError: String, Error {
    
    case Nil    = "Ошибка получения анкет"
    case Empty  = "У вас нет ни одной Анкеты"
    case Succes
  }
}


