
import FirebaseFirestoreSwift
import Foundation

class GETISympathys: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let userData = data as! CODUserData
    guard let fromID = userData.ID  else { return }
   
    guard let collection  = self.urls.createFB(type: .Sympathys(.sympathyList, fromID, .iSympathy))?.url else { return }
    
    collection.addSnapshotListener { (document, error) in
      guard let document = document else { return }
      do{
        let ankets = try document.documents.map{ try $0.data(as: CODAnket.self) }
        completion(.object(ankets))
      } catch let error {
        print(error.localizedDescription)
        completion(.error(error))
      }
    }
  }
}
