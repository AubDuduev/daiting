
import FirebaseFirestoreSwift
import Foundation

class GETChats: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let userData = data as! CODUserData
    guard let idUserFrom = userData.ID else { return }
    
    guard let collection = self.urls.createFB(type: .Chats(.chats, idUserFrom, .chatsUser))?.url else { return }

  
    collection.addSnapshotListener { (document, error) in
      guard let document = document else { return }
      do{
        let chats = try document.documents.map{ try $0.data(as: CODChat.self) }
        completion(.object(chats))
      } catch let error {
        print(error.localizedDescription)
        completion(.error(error))
      }
    }
  }
}

