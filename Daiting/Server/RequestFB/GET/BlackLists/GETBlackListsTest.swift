//
//  GETBlackListsTest.swift
//  Dating
//
//  Created by Senior Developer on 26.11.2020.
//
import FirebaseFirestoreSwift
import Foundation

class GETBlackListsTest: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateUser = data as! GDUpdateUser
    guard let fromID   = updateUser.userAnket.ID else { return }
    guard let testID   = updateUser.anket.ID   else { return }
    guard let document = self.urls.createFB(type: .BlackList(.blackList, fromID, .blackListUser, testID))?.doc else { return }
    
    document.addSnapshotListener { (document, error) in
      guard let document = document else { return }
      if document.exists {
        completion(.object(true))
      } else {
        completion(.object(false))
      }
    }
  }
}
