//
//  UPDATEUserData.swift

import FirebaseFirestoreSwift
import Foundation
import FirebaseAuth

class GETUserData: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  private let decode  = JSONDecoding()
  
  public func request(data: Any?, completion: @escaping ClousureRequest) {
    guard let email = data as? String else { return }
    print(email)
    guard let document = self.urls.createFB(type: .UserData(.usersFB, email))?.doc else { return }
    
    document.addSnapshotListener { (document, error) in
      
      if let document = document, document.exists {
        do {
          if let userData = try document.data(as: CODUserData.self) {
            completion(.object(userData))
          } else {
            print("Error parse CODUserData, class GETUserData")
          }
        } catch let error {
          completion(.error(error))
        }
      }
    }
  }
}
