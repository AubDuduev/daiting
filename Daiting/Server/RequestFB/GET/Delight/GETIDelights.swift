//
//  GETIDelights.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestoreSwift
import Foundation

class GETIDelights: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let userData = data as! CODUserData
    guard let fromID = userData.ID  else { return }
    
    guard let collection = self.urls.createFB(type: .Delights(.delightList, fromID, .iDelight))?.url else { return }
    
    collection.addSnapshotListener { (document, error) in
      guard let document = document else { return }
      do{
        let ankets = try document.documents.map{ try $0.data(as: CODAnket.self) }
        completion(.object(ankets))
      } catch let error {
        print(error.localizedDescription)
        completion(.error(error))
      }
    }
    
  }
}
