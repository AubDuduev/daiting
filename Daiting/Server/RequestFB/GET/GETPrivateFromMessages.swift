
import FirebaseFirestoreSwift
import Foundation

class GETPrivateFromMessages: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let sendMessage = data as! GDCurrentChat
    guard let fromID = sendMessage.fromUserData.ID else { return }
    guard let toID   = sendMessage.toUserData.ID   else { return }
  
    guard let collection = self.urls.createFB(type: .PrivateMessages(.privateMessages, fromID, toID))?.url else { return }
    
    
    collection.addSnapshotListener { (document, error) in
      guard let document = document else { return }
      do{
        let messages = try document.documents.map{ try $0.data(as: CODMessage.self) }
        completion(.object(messages))
      } catch let error {
        print(error.localizedDescription)
        completion(.error(error))
      }
    }
  }
}
