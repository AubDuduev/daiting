//
//  GETAnkects.swift

import FirebaseFirestoreSwift
import Foundation
import FirebaseAuth

class GETAnkects: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  private let decode  = JSONDecoding()
  
  public func request(data: Any?, completion: @escaping ClousureRequest) {
    
   
    guard let collection = self.urls.createFB(type: .Ankets(.ancetFB))?.url else { return }
    
    collection.getDocuments() { (document, err) in
      
      guard let document = document else { return }
      do{
        let ankets = try document.documents.map{ try $0.data(as: CODAnket.self) }
        completion(.object(ankets))
      } catch let error {
        print(error.localizedDescription)
        completion(.error(error))
      }
    }
  }
}
