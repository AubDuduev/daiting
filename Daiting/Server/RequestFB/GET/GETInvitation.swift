

import FirebaseFirestoreSwift
import Foundation

class GETInvitation: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let userData = data as! CODUserData
    guard let userDataID = userData.ID else { return }
    
    guard let document = self.urls.createFB(type: .Invitation(.invitation, userDataID))?.doc else { return }
    document.collection("Invitations").addSnapshotListener(includeMetadataChanges: true) { (document, error) in
      
      guard let document = document else { return }
      do{
        let ankets = try document.documents.map{ try $0.data(as: CODAnket.self) }
        completion(.object(ankets))
      } catch let error {
        print(error.localizedDescription)
        completion(.error(error))
      }
    }
  }
}
