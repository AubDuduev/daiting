//
//  DELETEDelight.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestoreSwift
import Foundation

class DELETEDelight: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateFavorite = data as! GDUpdateUser
    guard let fromID = updateFavorite.userAnket?.ID else { return }
    guard let toID   = updateFavorite.anket?.ID   else { return }
    guard let documentFrom = self.urls.createFB(type: .Delight(.delightList, fromID, .myDelight, toID))?.doc else { return }
    guard let documentTo   = self.urls.createFB(type: .Delight(.delightList, toID, .iDelight, fromID))?.doc else { return }
    
    documentFrom.delete(){ (error) in
      completion(.object(true))
    }
    documentTo.delete { (error) in
      completion(.object(true))
    }
  }
}

