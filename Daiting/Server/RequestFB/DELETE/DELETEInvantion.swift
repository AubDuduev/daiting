
import FirebaseFirestoreSwift
import Foundation

class DELETEInvantion: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let userData = data as! CODUserData
    guard let userDataID = userData.ID else { return }
    
    guard let document = self.urls.createFB(type: .Invitation(.invitation, userDataID))?.doc else { return }
    
    document.collection("Invitations").getDocuments { (snapShots, error) in
      guard let snapShots = snapShots else { return }
      for element in snapShots.documents {
        document.collection("Invitations").document(element.documentID).delete()
      }
      completion(.object(true))
    }
  }
}
