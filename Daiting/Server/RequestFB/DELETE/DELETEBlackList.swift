//
//  DELETEBlackList.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestoreSwift
import Foundation

class DELETEBlackList: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateUser = data as! GDUpdateUser
    guard let fromID = updateUser.userAnket.ID else { return }
    guard let toID   = updateUser.anket.ID   else { return }
   
    guard let document = self.urls.createFB(type: .NewBlackList(.blackList, fromID, .blackListUser, toID))?.doc else { return }
    
    document.delete(){ (error) in
      completion(.object(true))
    }
  }
}
