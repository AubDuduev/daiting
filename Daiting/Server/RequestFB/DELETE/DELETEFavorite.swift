//
//  DELETEFavorite.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestoreSwift
import Foundation

class DELETEFavorite: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateUser = data as! GDUpdateUser
    guard let fromID   = updateUser.userAnket.ID  else { return }
    guard let toIDid   = updateUser.anket.ID else { return }
    guard let document = self.urls.createFB(type: .Favorite(.favoriteList, fromID, .favorites, toIDid))?.doc else { return }
    
    document.delete(){ (error) in
      completion(.object(true))
    }
  }
}
