
import FirebaseFirestoreSwift
import Foundation

class DELETESympathy: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateFavorite = data as! GDUpdateUser
    guard let fromID = updateFavorite.userAnket?.ID else { return }
    guard let toID   = updateFavorite.anket?.ID   else { return }
    guard let documentFrom = self.urls.createFB(type: .Sympathy(.sympathyList, fromID, .mySympathy, toID))?.doc else { return }
    guard let documentTo   = self.urls.createFB(type: .Sympathy(.sympathyList, toID, .iSympathy, fromID))?.doc else { return }
    
    documentFrom.delete(){ (error) in
      completion(.object(true))
    }
    documentTo.delete { (error) in
      completion(.object(true))
    }
  }
}
