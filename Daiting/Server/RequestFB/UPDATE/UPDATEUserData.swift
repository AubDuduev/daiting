//
//  UPDATEUserData.swift
import Protocols
import FirebaseFirestoreSwift
import Foundation

class UPDATEUserData: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    guard let userData = data as? CODUserData else { return }
    guard let document = self.urls.createFB(type: .UserData(.usersFB, userData.email))?.doc else { return }
    
    do {
      try document.setData(from: userData)
      completion(.object(true))
    } catch let error {
      completion(.error(error))
      print(error.localizedDescription)
    }
    
  }
}
