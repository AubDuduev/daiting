
import FirebaseFirestoreSwift
import Foundation

class POSTBlackList: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let updateUser = data as! GDUpdateUser
    guard let fromID = updateUser.userAnket.ID else { return }
    guard let toID   = updateUser.anket.ID     else { return }
   
    guard let document = self.urls.createFB(type: .BlackList(.blackList, fromID, .blackListUser, toID))?.doc else { return }
    
    do {
      let _ = try document.setData(from: updateUser.anket)
      completion(.object(updateUser.anket!)) 
    } catch let error {
      print(error.localizedDescription)
    }
  }
}
