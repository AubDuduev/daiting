
import FirebaseFirestoreSwift
import Foundation

class POSTUsers: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let user   = (data as! CODUserData)
    guard let userID = user.ID else { return }
    guard let document = self.urls.createFB(type: .Users(.ancetFB, userID))?.doc else { return }
    
    do {
      let _ = try document.setData(from: user)
    } catch let error {
      print(error.localizedDescription)
    }
  }
}
