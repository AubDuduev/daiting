
import FirebaseFirestoreSwift
import Foundation

class POSTUserData: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    let user = data as! CODUserData
    guard let doc = self.urls.createFB(type: .UserData(.usersFB, user.email))?.doc else { return }
    do {
      try doc.setData(from: user) 
      completion(.object(user))
    } catch let error {
      completion(.error(error))
    }
  }
}
