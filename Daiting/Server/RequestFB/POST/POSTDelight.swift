
import FirebaseFirestoreSwift
import Foundation

class POSTDelight: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateUser = data as! GDUpdateUser
    guard let fromID = updateUser.userAnket.ID  else { return }
    guard let toID   = updateUser.anket.ID else { return }
    guard let documentFrom = self.urls.createFB(type: .Delight(.delightList, fromID, .myDelight, toID))?.doc else { return }
    guard let documentTo   = self.urls.createFB(type: .Delight(.delightList, toID, .iDelight, fromID))?.doc else { return }
    
    do {
      try documentFrom.setData(from: updateUser.anket)
      completion(.object(true))
    } catch let error {
      completion(.error(error))
      print(error.localizedDescription)
    }
    
    do {
      try documentTo.setData(from: updateUser.userAnket)
      completion(.object(true))
    } catch let error {
      completion(.error(error))
      print(error.localizedDescription)
    }
  }
}
