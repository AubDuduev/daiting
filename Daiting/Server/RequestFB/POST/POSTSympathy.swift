
import FirebaseFirestoreSwift
import Foundation

class POSTSympathy: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
   
    let updateUser = data as! GDUpdateUser
    guard let fromID = updateUser.userAnket.ID else { return }
    guard let toID   = updateUser.anket.ID   else { return }
    guard let documentFrom = self.urls.createFB(type: .Sympathy(.sympathyList, fromID, .mySympathy, toID))?.doc else { return }
    guard let documentTo   = self.urls.createFB(type: .Sympathy(.sympathyList, toID, .iSympathy, fromID))?.doc else { return }
    
    do {
      try documentFrom.setData(from: updateUser.anket)
      completion(.object(true))
    } catch let error {
      completion(.error(error))
      print(error.localizedDescription)
    }
    do {
      try documentTo.setData(from: updateUser.userAnket)
      completion(.object(true))
    } catch let error {
      completion(.error(error))
      print(error.localizedDescription)
    }
  }
}
