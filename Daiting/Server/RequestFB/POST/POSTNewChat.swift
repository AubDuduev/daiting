
import FirebaseFirestoreSwift
import Foundation

class POSTNewChat: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let sendMessage = data as! GDSendMessages
    guard let fromID = sendMessage.fromUserData.ID else { return }
    guard let toID   = sendMessage.toUserData.ID   else { return }
    let codChat = CODChat(userID: toID)
    guard let document = self.urls.createFB(type: .NewChats(.chats, fromID, .chatsUser, toID))?.doc else { return }
    
    do {
      let _ = try document.setData(from: codChat)
    } catch let error {
      print(error.localizedDescription)
    }
  }
}

