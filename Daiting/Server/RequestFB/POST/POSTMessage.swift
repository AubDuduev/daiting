
import FirebaseFirestoreSwift
import Foundation

class POSTMessage: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let sendMessage = data as! GDSendMessages
    guard let fromID = sendMessage.fromUserData.ID else { return }
    guard let toID   = sendMessage.toUserData.ID   else { return }
    
    guard let collection = self.urls.createFB(type: .Messages(.messages, fromID, toID))?.url else { return }
    
    do {
      let _ = try collection.addDocument(from: sendMessage.message)
    } catch let error {
      print(error.localizedDescription)
    }
  }
}
