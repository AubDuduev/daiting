
import FirebaseFirestoreSwift
import Foundation
import FirebaseStorage

class POSTImage: RequestobleFB {
  
  private let urls      = URLs()
  private let urlBody   = URLBody()
  private let reference = Storage.storage()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let url       = self.reference.reference().child("\(UUID().uuidString).jpg")
    let dataImage = data as! Data
    
    url.putData(dataImage, metadata: nil) { (metadata, error) in
      guard let _ = metadata else {
        print(metadata as Any)
        return
      }
      // You can also access to download URL after upload.
      url.downloadURL { (url, error) in
        
        if let error = error {
          completion( .error(error))
        } else {
          completion(.object(url!.absoluteString))
        }
      }
    }
  }
}
