
import FirebaseFirestoreSwift
import Foundation

class POSTInvitation: RequestobleFB {
  
  private let urls    = URLs()
  private let urlBody = URLBody()
  
  func request(data: Any?, completion: @escaping ClousureRequest) {
    
    let invitation = data as! GDInvitation
    guard let fromID = invitation.selfUser.ID else { return }
    guard let toID   = invitation.toUserData.ID   else { return }
    
    guard let document = self.urls.createFB(type: .Invitation(.invitation, toID))?.doc else { return }
     
    do {
      let _ = try document.collection("Invitations").addDocument(from: invitation.selfUser)
    } catch let error {
      print(error.localizedDescription)
    }
  }
}
 
