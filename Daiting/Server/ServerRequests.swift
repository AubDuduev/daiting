
import Foundation

class ServerRequests {
  
  //MARK: - Create type request
  public func set(type: RequestType) -> Requestoble? {
    switch type {
      //GETs
      case .GET:
        return nil
      //POSTs
      case .POST:
        return nil
      //PATCHs
      case .PATCH:
        return nil
    }
  }
  
  enum RequestType {
    //GET
    case GET
    //POST
    case POST
    //PATCH
    case PATCH
  }
}
