//
//  CODBlackList.swift
//  Dating
//
//  Created by Senior Developer on 11.11.2020.
//
import Foundation

struct CODBlackList {
  
  let userID: String!
  
  enum CodingKeys: String, CodingKey {
    
    case userID
    
  }
}
extension CODBlackList: Codable {
  
  init(from decoder: Decoder) throws {
    
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.userID  = try? values.decode(String?.self, forKey: .userID)
   
  }
}
