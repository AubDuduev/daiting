//
//  DECAnket.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import Foundation

struct CODAnket: Hashable {
  
  let ID          : String!
  let email       : String!
  let avatar      : String!
  let name        : String!
  let city        : String!
  let age         : String!
  let gender      : String!
  let searchGender: String!
  let searchCity  : String!
  let searchAge   : String!
  var isLike      : Bool!
  var isDelight   : Bool!
  var blackList   : Bool!
  
  enum CodingKeys: String, CodingKey {
    
    case ID
    case email
    case name
    case city
    case age
    case gender
    case searchGender
    case searchCity
    case searchAge
    case avatar
    case isLike
    case isDelight
    case blackList
  }
}
extension CODAnket: Codable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.ID           = try? values.decode(String?.self, forKey: .ID)
    self.email        = try? values.decode(String?.self, forKey: .email)
    self.name         = try? values.decode(String?.self, forKey: .name)
    self.city         = try? values.decode(String?.self, forKey: .city)
    self.age          = try? values.decode(String?.self, forKey: .age)
    self.gender       = try? values.decode(String?.self, forKey: .gender)
    self.searchGender = try? values.decode(String?.self, forKey: .searchGender)
    self.searchCity   = try? values.decode(String?.self, forKey: .searchCity)
    self.searchAge    = try? values.decode(String?.self, forKey: .searchAge)
    self.avatar       = try? values.decode(String?.self, forKey: .avatar)
    self.isDelight    = try? values.decode(Bool?.self  , forKey: .isDelight)
    self.isLike       = try? values.decode(Bool?.self  , forKey: .isLike)
    self.blackList    = try? values.decode(Bool?.self  , forKey: .blackList)
  }
}

extension CODAnket {
  
  init(mutually: GDMutually) {
    
    self.ID           = mutually.ID
    self.email        = mutually.email
    self.avatar       = mutually.avatar
    self.name         = mutually.name
    self.city         = mutually.city
    self.age          = mutually.age
    self.gender       = mutually.gender
    self.searchGender = mutually.searchGender
    self.searchCity   = mutually.searchCity
    self.searchAge    = mutually.searchAge
    self.isLike       = mutually.isSympathy
    self.isDelight    = mutually.isDelight
    self.blackList    = mutually.blackList
  }
}

extension CODAnket {
  
  init(userData: CODUserData) {
    
    self.ID           = userData.ID
    self.email        = userData.email
    self.avatar       = userData.avatar
    self.name         = userData.name
    self.city         = userData.city
    self.age          = userData.age
    self.gender       = userData.gender
    self.searchGender = userData.searchGender
    self.searchCity   = userData.searchCity
    self.searchAge    = userData.searchAge
    self.isLike   = userData.isSympathy
    self.isDelight    = userData.isDelight
    self.blackList    = userData.blackList
  }
}
