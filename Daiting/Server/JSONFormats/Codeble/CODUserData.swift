//
//  CODUserData.swift

import Foundation

struct CODUserData: Codable {
  
  let ID          : String!
  var email       : String!
  var name        : String!
  var city        : String!
  var age         : String!
  var gender      : String!
  var status      : String!
  var searchGender: String!
  var searchCity  : String!
  var searchAge   : String!
  var avatar      : String!
  var isSympathy  : Bool!
  var isDelight   : Bool!
  var blackList   : Bool!
  
  init(ID: String?, email: String = "Введите свой емаил", name: String = "Имя", city: String = "", age: String = "", searchCity: String = "", searchAge: String = "", gender: String = "", searchGender: String = "", avatar: String = "", status: String = ""){
    self.ID           = ID
    self.email        = email
    self.name         = name
    self.city         = city
    self.age          = age
    self.searchCity   = searchCity
    self.searchAge    = searchAge
    self.gender       = gender
    self.status       = status
    self.searchGender = searchGender
    self.avatar       = avatar
  }
}

extension CODUserData {
  
  init(anket: CODAnket) {
    
    self.ID           = anket.ID
    self.email        = anket.email
    self.avatar       = anket.avatar
    self.name         = anket.name
    self.city         = anket.city
    self.age          = anket.age
    self.gender       = anket.gender
    self.searchGender = anket.searchGender
    self.searchCity   = anket.searchCity
    self.searchAge    = anket.searchAge
    self.isSympathy   = anket.isLike
    self.isDelight    = anket.isDelight
    self.blackList    = anket.blackList
  }
}

