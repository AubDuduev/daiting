//
//  CODChat.swift
//  Dating
//
//  Created by Senior Developer on 07.11.2020.
//
import Foundation

struct CODChat {
  
  let userID: String!
  
  enum CodingKeys: String, CodingKey {
    
    case userID
    
  }
}
extension CODChat: Codable {
  
  init(from decoder: Decoder) throws {
    
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.userID  = try? values.decode(String?.self, forKey: .userID)
   
  }
}
