//
//  CODMessages.swift

import Foundation

struct CODMessage {
  
  let text  : String!
  let data  : Double!
  let fromID: String!
  
  enum CodingKeys: String, CodingKey {
    
    case text
    case data
    case fromID
  }
}
extension CODMessage: Codable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.text   = try? values.decode(String?.self, forKey: .text)
    self.data   = try? values.decode(Double?.self, forKey: .data)
    self.fromID = try? values.decode(String?.self, forKey: .fromID)
  }
}
