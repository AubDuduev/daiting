
import UIKit

class ServerFB {
  
  //MARK: - Public variable
  public let urls = URLs()
  
  //MARK: - Private variable
  private var requestoble : RequestobleFB!
  private let network     = Network()
  private let requests    = ServerRequestsFB()
  
  //MARK: - Request Server
  public func request(requestType: ServerRequestsFB.Types, data: Any? = nil, completion: @escaping ClousureServerResult){
    
    //check network
    guard self.network.check() else { return }
    
    //create type request
    requestoble = self.requests.set(type: requestType)
    
    //request
    requestoble.request(data: data) { (requestResult) in
      //responce
      switch requestResult{
        //susses
        case .object(let json):
          DispatchQueue.main.async {
            completion(.object(json))
        }
        //error
        case .error(let error):
          DispatchQueue.main.async {
            completion(.error(error))
        }
      }
    }
  }
}









