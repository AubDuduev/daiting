//
//  URLBody.swift
//  DgBetTrip
//
//  Created by Senior Developer on 01.05.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import Foundation

class URLBody {
  
  enum Types {
     
     case Some
   }
  
  public func create(type: Types?) -> Data? {
    guard let type = type else { return nil }
    switch type {
      case .Some:
        return nil
    }
  }
}
