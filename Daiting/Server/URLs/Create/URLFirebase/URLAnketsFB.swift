//
//  URLUserFB.swift

import FirebaseFirestore
import Foundation

class URLAnketsFB: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Ankets(let anketsFB):
        let ankets = reference.collection(anketsFB.rawValue)
        return (ankets, nil)
      default:
        return nil
    }
  }
}
