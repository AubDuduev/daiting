//
//  URLGetBlackLists.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetBlackLists: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .BlackLists(let users, let email, let blackLists):
        let blackLists = reference.collection(users.rawValue).document(email).collection(blackLists.rawValue)
        return (blackLists, nil)
      default:
        return nil
    }
  }
}
