//
//  URLGetBlackList.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetBlackList: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .BlackList(let blackList, let fromUserID, let blackLists, let toID):
        let document = reference.collection(blackList.rawValue).document(fromUserID).collection(blackLists.rawValue).document(toID)
        return (nil, document)
      default:
        return nil
    }
  }
}
