//
//  URLGetFavorite.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetFavorite: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Favorite(let users, let email, let favorites, let id):
        let favorites = reference.collection(users.rawValue).document(email).collection(favorites.rawValue).document(id)
        return (nil, favorites)
      default:
        return nil
    }
  }
}
