
import FirebaseFirestore
import Foundation

class URLGetMessaages: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Messages(let messages, let fromUserID, let toUserID):
        let messagesURL = reference.collection(messages.rawValue).document(fromUserID).collection(toUserID)
        return (messagesURL, nil)
      default:
        return nil
    }
  }
}
