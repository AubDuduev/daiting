//
//  URLGetSympathy.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetSympathy: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Sympathy(let sympathyList, let fromID, let sympathys, let toID):
        let document = reference.collection(sympathyList.rawValue).document(fromID).collection(sympathys.rawValue).document(toID)
        return (nil, document)
      default:
        return nil
    }
  }
}
