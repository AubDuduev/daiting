//
//  URLGetSympathys.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetSympathys: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Sympathys(let sympathyList, let fromID, let sympathys):
        let sympathys = reference.collection(sympathyList.rawValue).document(fromID).collection(sympathys.rawValue)
        return (sympathys, nil)
      default:
        return nil
    }
  }
}
