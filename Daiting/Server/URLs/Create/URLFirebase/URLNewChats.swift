//
//  URLNewChats.swift
//  Dating
//
//  Created by Senior Developer on 11.11.2020.
//
import FirebaseFirestore
import Foundation

class URLNewChats: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .NewChats(let chats, let fromUserID, let chatUsers, let toUserId):
        let document = reference.collection(chats.rawValue).document(fromUserID).collection(chatUsers.rawValue).document(toUserId)
        return (nil, document)
      default:
        return nil
    }
  }
}
