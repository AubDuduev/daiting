
import FirebaseFirestore
import Foundation

class URLInvitation: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Invitation(let invitation, let fromUserID):
        let document = reference.collection(invitation.rawValue).document(fromUserID)
        return (nil, document)
      default:
        return nil
    }
  }
}
