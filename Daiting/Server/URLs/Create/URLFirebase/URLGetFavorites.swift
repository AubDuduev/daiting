//
//  URLGetFavorites.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetFavorites: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Favorites(let users, let email, let favorites):
        let favorites = reference.collection(users.rawValue).document(email).collection(favorites.rawValue)
        return (favorites, nil)
      default:
        return nil
    }
  }
}
