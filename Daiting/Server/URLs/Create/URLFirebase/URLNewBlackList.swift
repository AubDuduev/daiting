//
//  URLNewBlackList.swift
//  Dating
//
//  Created by Senior Developer on 11.11.2020.
//
import FirebaseFirestore
import Foundation

class URLNewBlackList: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .NewBlackList(let blackList, let fromUserID, let blackListUser, let toUserId):
        let document = reference.collection(blackList.rawValue).document(fromUserID).collection(blackListUser.rawValue).document(toUserId)
        return (nil, document)
      default:
        return nil
    }
  }
}
