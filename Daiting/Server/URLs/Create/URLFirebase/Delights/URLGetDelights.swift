//
//  URLGetDelights.swift
//  Dating
//
//  Created by Senior Developer on 06.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetDelights: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Delights(let delightList, let fromID, let delights):
        let mutuallys = reference.collection(delightList.rawValue).document(fromID).collection(delights.rawValue)
        return (mutuallys, nil)
      default:
        return nil
    }
  }
}
