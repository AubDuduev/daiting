
import FirebaseFirestore
import Foundation

class URLGetDelight: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Delight(let delightList, let fromID, let delights, let toID):
        let mutuallys = reference.collection(delightList.rawValue).document(fromID).collection(delights.rawValue).document(toID)
        return (nil, mutuallys)
      default:
        return nil
    }
  }
}
