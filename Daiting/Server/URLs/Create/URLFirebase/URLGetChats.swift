//
//  URLGetChats.swift
//  Dating
//
//  Created by Senior Developer on 07.11.2020.
//
import FirebaseFirestore
import Foundation

class URLGetChats: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Chats(let chats, let fromUserID, let chatsUser):
        let collection = reference.collection(chats.rawValue).document(fromUserID).collection(chatsUser.rawValue)
        return (collection, nil)
      default:
        return nil
    }
  }
}
