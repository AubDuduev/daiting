
import FirebaseFirestore
import Foundation

class URLPostUsers: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .Users(let anketsFB, let userID):
        let anketsDoc = reference.collection(anketsFB.rawValue).document(userID)
        return (nil, anketsDoc)
      default:
        return nil
    }
  }
}
