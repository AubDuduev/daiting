//
//  URLUserDataFB.swift
//  Games
//
//  Created by Developer on 25.05.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import FirebaseFirestore
import Foundation

class URLUserDataFB: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .UserData(let users, let email):
        let document   = reference.collection(users.rawValue).document(email)
        let collection = reference.collection(users.rawValue)
        return (collection, document)
      default:
        return nil
    }
  }
}

