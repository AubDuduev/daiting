
import FirebaseFirestore
import Foundation

class URLGetPrivateMessages: URLCreatobleFB {
  
  private let reference = Firestore.firestore()
  
  func url(_ type: URLTypeFB.Types) -> ReturnURLFB? {
    switch type {
      case .PrivateMessages(let privateMessages, let fromUserID, let toUserID):
        let messagesURL = reference.collection(privateMessages.rawValue).document(fromUserID).collection(toUserID)
        return (messagesURL, nil)
      default:
        return nil
    }
  }
}
