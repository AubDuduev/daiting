//
//  URLPath.swift
//  DgBetTrip
//
//  Created by Senior Developer on 19.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import Foundation

class URLPath {
  
  private let currentUserID =  "no userd id"
  
  enum Path: String {

    case non
    case array
    case favoriteList     = "FavoriteList"
    case favorites
    case blackList        = "BlackList"
    case iDelight
    case myDelight
    case iSympathy
    case mySympathy
    case sympathyList     = "SympathyList"
    case delightList      = "DelightList"
    case chatsUser
    case blackListUser
    case privateMessages  = "PrivateMessages"
    case invitation       = "Invitation"
    case messages         = "Messages"
    case users
    case usersFB          = "NewUsers"
    case ancetFB          = "Users"
    case urls             = "URLs"
    case advertising      = "urlAdvertising"
    case advertisingError = "urlAdvertisingError"
    case chats            = "Chats"
  }
  enum ChangePath {
    
    case non
  }
  
  public func create(change: ChangePath) -> String {
    //create Change Path for url
    switch change {
      //MARK: - Non
      case .non:
        return ""
    }
  }
}
