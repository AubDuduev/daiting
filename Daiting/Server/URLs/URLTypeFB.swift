//
import Foundation

class URLTypeFB {
  
  var creatoble: URLCreatobleFB!
  
  public func set(_ type: Types) -> URLCreatobleFB {
    switch type {
      case .UserData:
        return URLUserDataFB()
      case .Ankets:
        return URLAnketsFB()
      case .Messages:
        return URLGetMessaages()
      case .Favorite:
        return URLGetFavorite()
      case .Favorites:
        return URLGetFavorites()
      case .NewBlackList:
        return URLNewBlackList()
      case .NewChats:
        return URLNewChats()
      case .Chats:
        return URLGetChats()
      case .Users:
        return URLPostUsers()
      case .PrivateMessages:
        return URLGetPrivateMessages()
      case .Invitation:
        return URLInvitation()
      //BlackLists
      case .BlackLists:
        return URLGetBlackLists()
      case .BlackList:
        return URLGetBlackList()
      //Delights
      case .DelightTest:
        return URLGetDelight()
      case .Delight:
        return URLGetDelight()
      case .Delights:
        return URLGetDelights()
      //Sympathys
      case .SympathyTest:
        return URLGetSympathy()
      case .Sympathy:
        return URLGetSympathy()
      case .Sympathys:
        return URLGetSympathys()
    }
  }
  enum Types {
    case Ankets(URLPath.Path)
    case UserData(URLPath.Path, String)
    case Messages(URLPath.Path, String, String)
    case Invitation(URLPath.Path, String)
    case PrivateMessages(URLPath.Path, String, String)
    case Chats(URLPath.Path, String, URLPath.Path)
    case NewChats(URLPath.Path, String, URLPath.Path, String)
    case NewBlackList(URLPath.Path, String, URLPath.Path, String)
    case Favorite(URLPath.Path, String, URLPath.Path, String)
    case Favorites(URLPath.Path, String, URLPath.Path)
    case Sympathy(URLPath.Path, String, URLPath.Path, String)
    case Sympathys(URLPath.Path, String, URLPath.Path)
    case Delight(URLPath.Path, String, URLPath.Path, String)
    case Delights(URLPath.Path, String, URLPath.Path)
    case DelightTest(URLPath.Path, String, URLPath.Path)
    case BlackLists(URLPath.Path, String, URLPath.Path)
    case BlackList(URLPath.Path, String, URLPath.Path, String)
    case Users(URLPath.Path, String)
    case SympathyTest(URLPath.Path, String, URLPath.Path, String)
  }
}
