//
//  URLString.swift
//  DgBetTrip
//
//  Created by Senior Developer on 19.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import Foundation

class URLHost {
  
  enum Host: String {
    case non
  }
  
  public func create(_ type: URLHost.Host) -> String {
    switch type {
      case .non:
        return ""
    }
  }
}
