
import Foundation

class ServerRequestsFB {
  
  //MARK: - Create type request
  public func set(type: Types) -> RequestobleFB? {
    switch type {
      //GETs
      case .GETUserData:
        return GETUserData()
      case .GETFromMessages:
        return GETFromMessages()
      case .GETToMessages:
        return GETToMessages()
      case .GETPrivateToMessages:
        return GETPrivateToMessages()
      case .GETPrivateFromMessages:
        return GETPrivateFromMessages()
      case .GETAnkets:
        return GETAnkects()
      case .GETFavorites:
        return GETFavorites()
      case .GETMySympathys:
        return GETMySympathys()
      case .GETISympathys:
        return GETISympathys()
      case .GETSympathyTest:
        return GETSympathyTest()
      case .GETIDelights:
        return GETIDelights()
      case .GETMyDelights:
        return GETMyDelights()
      case .GETDelightTest:
        return GETDelightTest()
      case .GETBlackLists:
        return GETBlackLists()
      case .GETBlackListsTest:
        return GETBlackListsTest()
      case .GETChats:
        return GETChats()
      case .GETInvitation:
        return GETInvitation()
      //POSTs
      case .POSTUserData:
        return POSTUserData()
      case .POSTMessage:
        return POSTMessage()
      case .POSTFavorite:
        return POSTFavorite()
      case .POSTSympathy:
        return POSTSympathy()
      case .POSTDelight:
        return POSTDelight()
      case .POSTBlackList:
        return POSTBlackList()
      case .POSTNewChat:
        return POSTNewChat()
      case .POSTImage:
        return POSTImage()
      case .POSTUsers:
        return POSTUsers()
      case .POSTPrivateMessage:
        return POSTPrivateMessage()
      case .POSTInvitation:
        return POSTInvitation()
      //DELETE
      case .DELETEFavorite:
        return DELETEFavorite()
      case .DELETESympathy:
        return DELETESympathy()
      case .DELETEDelight:
        return DELETEDelight()
      case .DELETEBlackList:
        return DELETEBlackList()
      case .DELETEInvantion:
        return DELETEInvantion()
      //PATCHs
      case .PATCH:
        return nil
      //UPDATE
      case .UPDATEUserData:
        return UPDATEUserData()
     
    }
  }
  
  enum Types {
    //GET
    case GETUserData
    case GETToMessages
    case GETFromMessages
    case GETPrivateFromMessages
    case GETPrivateToMessages
    case GETAnkets
    case GETFavorites
    case GETMySympathys
    case GETISympathys
    case GETSympathyTest
    case GETIDelights
    case GETMyDelights
    case GETDelightTest
    case GETBlackLists
    case GETChats
    case GETBlackListsTest
    case GETInvitation
    //POST
    case POSTUserData
    case POSTMessage
    case POSTFavorite
    case POSTSympathy
    case POSTDelight
    case POSTNewChat
    case POSTBlackList
    case POSTImage
    case POSTUsers
    case POSTPrivateMessage
    case POSTInvitation
    //PATCH
    case PATCH
    //DELETE
    case DELETEBlackList
    case DELETESympathy
    case DELETEDelight
    case DELETEFavorite
    case DELETEInvantion
    //UPDATE
    case UPDATEUserData
   
  }
}
