
import UIKit

class AnanimusTable: NSObject {
  
  public var viewModal: AnanimusViewModal?
  public var tableView: UITableView!
  public var messages : [CODMessage]!
  
}
//MARK: - Delegate
extension AnanimusTable: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
  }
}
//MARK: - DataSources
extension AnanimusTable: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    self.tableView = tableView
    return self.messages?.count ?? 0
  }
  func numberOfSections(in tableView: UITableView) -> Int {
    self.tableView = tableView
    return 1
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = AnanimusTableCell().tableCell()
    cell.configure(viewModal: self.viewModal, message: messages?[indexPath.row])
  return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    return tableView.estimatedRowHeight
  }
  
}
