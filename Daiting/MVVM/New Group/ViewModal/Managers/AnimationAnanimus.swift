import UIKit
import Protocols

class AnimationAnanimus: VMManager {
  
  //MARK: - Public variable
  public var VM: AnanimusViewModal!
  
  @objc
  public func animationSendMessageView(notification: Notification?, show: Bool){
    guard let userInfo = notification?.userInfo else { return }
    let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
    UIView.animate(withDuration: 0.1) {
      if show {
        let difference = keyboardFrame.height - self.VM.VC.view.safeAreaInsets.bottom
        self.VM.VC.sendMessageView.transform = CGAffineTransform(translationX: 0, y: -difference)
      } else {
        self.VM.VC.sendMessageView.transform = .identity
      }
      self.VM.VC.view.layoutIfNeeded()
    }
  }
}
//MARK: - Initial
extension AnimationAnanimus {
  
  //MARK: - Inition
  convenience init(viewModal: AnanimusViewModal) {
    self.init()
    self.VM = viewModal
  }
}

