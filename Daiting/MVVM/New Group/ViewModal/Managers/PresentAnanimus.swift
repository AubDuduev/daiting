
import UIKit
import Protocols

class PresentAnanimus: VMManager {
  
  //MARK: - Public variable
  public var VM: AnanimusViewModal!
  
  public func messagesTable(messages: [CODMessage]?){
    self.VM.VC.ananimusTable.messages = messages
    self.VM.VC.ananimusTable.tableView.reloadData()
    self.VM.VC.ananimusTable.tableView.scrollTo(.bottom)
  }
}
//MARK: - Initial
extension PresentAnanimus {
  
  //MARK: - Inition
  convenience init(viewModal: AnanimusViewModal) {
    self.init()
    self.VM = viewModal
  }
}

