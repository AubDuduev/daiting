
import UIKit
import Protocols

class SetupAnanimus: VMManager {
  
  //MARK: - Public variable
  public var VM: AnanimusViewModal!
  
  public func addNotification(){
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.animation.animationSendMessageView(notification: notification, show: true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.animation.animationSendMessageView(notification: notification, show: false)
    }
  }
  public func ananimusTable(){
    self.VM.VC.ananimusTable.viewModal = self.VM
    self.VM.VC.ananimusTable.tableView = self.VM.VC.ananimusTableView
  }
  public func sendMessageCornerView(){
    self.VM.VC.sendMessageCornerView.cornerRadius(5, true)
  }
}
//MARK: - Initial
extension SetupAnanimus {
  
  //MARK: - Inition
  convenience init(viewModal: AnanimusViewModal) {
    self.init()
    self.VM = viewModal
  }
}


