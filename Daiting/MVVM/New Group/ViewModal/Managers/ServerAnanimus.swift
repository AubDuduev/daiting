import UIKit
import Protocols

class ServerAnanimus: VMManager {
  
  //MARK: - Public variable
  public var VM: AnanimusViewModal!
  
  public func getFromMessages(data: GDCurrentChat, completion: @escaping Clousure<[CODMessage]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETPrivateFromMessages, data: data) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChats ->, function: getFromMessages -> data: [CODMessage]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getFromMessages(data: data, completion: completion)
          }
        //Susses
        case .object(let object):
          let messages = object as? [CODMessage]
          completion(messages)
          print("Succesful data: class: ServerChats ->, function: getFromMessages ->, data: [CODMessage]?")
          
      }
    }
  }
  public func getToMessages(data: GDCurrentChat, completion: @escaping Clousure<[CODMessage]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETPrivateToMessages, data: data) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChats ->, function: getToMessages -> data: [CODMessage]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getToMessages(data: data, completion: completion)
          }
        //Susses
        case .object(let object):
          let messages = object as? [CODMessage]
          completion(messages)
          print("Succesful data: class: ServerChats ->, function: getToMessages ->, data: [CODMessage]?")
          
      }
    }
  }
  public func addLike(updateUser: GDUpdateUser){
    //Request
    self.VM.serverFB.request(requestType: .POSTSympathy, data: updateUser) { (serverResult) in
      
    }
  }
  public func postMessage(data: GDSendMessages){
    //Request
    self.VM.serverFB.request(requestType: .POSTPrivateMessage, data: data) { (serverResult) in
    }
  }
}
//MARK: - Initial
extension ServerAnanimus {
  
  //MARK: - Inition
  convenience init(viewModal: AnanimusViewModal) {
    self.init()
    self.VM = viewModal
  }
}


