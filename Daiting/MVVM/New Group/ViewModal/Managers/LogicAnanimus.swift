import UIKit
import Protocols

class LogicAnanimus: VMManager {
  
  //MARK: - Public variable
  public var VM: AnanimusViewModal!
  
  public func setCurrentChat(anket: CODAnket?){
    guard let anket = anket else { return }
    self.VM.currentChat = GDCurrentChat(fromUserData: GVuserData, toUserData: anket)
  }
  public func getData(){
    self.VM.ananimusModal = .loading
  }
  public func sendMessage(){
    let text    = self.VM.VC.sendTextField.text
    let message = CODMessage(text  : text,
                             data  : Date().timeIntervalSince1970,
                             fromID: self.VM.currentChat.fromUserData.ID)
    let sendMessage = GDSendMessages(fromUserData: self.VM.currentChat.fromUserData,
                                     toUserData  : self.VM.currentChat.toUserData,
                                     message     : message)
    self.VM.managers.server.postMessage(data: sendMessage)
    //self.VM.managers.server.postNewChat(data: sendMessage)
    self.VM.VC.sendTextField.text?.removeAll()
  }
  public func resignFirstResponder(){
    self.VM.VC.sendTextField.resignFirstResponder()
  }
  public func addLike(){
    let userAnket  = CODAnket(userData: self.VM.currentChat.fromUserData)
    let updateUser = GDUpdateUser(userAnket: userAnket,
                                  anket    : self.VM.currentChat.toUserData)
    self.VM.managers.server.addLike(updateUser: updateUser)
    self.VM.VC.dismiss(animated: true)
  }
  public func timerAnsver(){
    var time = 30
    self.VM.timerAnsver = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (_) in
      guard let self = self else { return }
      self.VM.VC.timerAnsverLabel.text = String(time)
      time -= 1
      guard time == 0 else { return }
      self.timerDissmiss()
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
        guard let self = self else { return }
        self.VM.VC.dismiss(animated: true, completion: nil)
      }
    })
  }
  public func timerDissmiss(){
    self.VM.timerAnsver?.invalidate()
    UIView.animate(withDuration: 1) {
      self.VM.VC.timerAnsverView.isHidden = true
      self.VM.VC.view.layoutIfNeeded()
    }
  }
}
//MARK: - Initial
extension LogicAnanimus {
  
  //MARK: - Inition
  convenience init(viewModal: AnanimusViewModal) {
    self.init()
    self.VM = viewModal
  }
}
