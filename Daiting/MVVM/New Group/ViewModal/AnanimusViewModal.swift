
import Foundation
import Protocols
import SwiftEntryKit

class AnanimusViewModal: VMManagers {
	
	public var ananimusModal: AnanimusModal = .loading {
		didSet{
			self.ananimusModalLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers    : AnanimusManagers!
  public var VC          : AnanimusViewController!
  public var serverFB    = ServerFB()
  public var messages    = [CODMessage]()
  public var currentChat : GDCurrentChat!
  public var timerAnsver : Timer!
  
  public func viewDidLoad() {
    self.managers.setup.addNotification()
    self.managers.setup.ananimusTable()
    self.managers.logic.setCurrentChat(anket: self.VC.ananimusData.anket)
    self.managers.logic.getData()
  }
  public func viewDidAppear() {
    self.managers.logic.timerAnsver()
    SwiftEntryKit.dismiss()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.sendMessageCornerView()
  }
  
  public func ananimusModalLogic(){
    
    switch self.ananimusModal {
      //1 - Загрузка
      case .loading:
        
        self.ananimusModal = .getData(self.currentChat)
      //2 - Получаем данные
      case .getData(let currentChat):
        
        //Полчуаем исходящие сообщания
        self.managers.server.getFromMessages(data: currentChat) { (messages) in
          
          var toMessages = self.messages.filter{ $0.fromID != currentChat.fromUserData.ID}
          toMessages.append(contentsOf: messages!)
          self.messages = toMessages.sorted{ ($0.data ?? 0.0) < ($1.data ?? 0.0) }
          self.ananimusModal = .errorHandler(self.messages)
        }
        //Полчуаем входящие сообщания
        self.managers.server.getToMessages(data: currentChat) { (messages) in
          
          var fromMessages = self.messages.filter{ $0.fromID != currentChat.toUserData.ID}
          fromMessages.append(contentsOf: messages!)
          self.messages = fromMessages.sorted{ ($0.data ?? 0.0) < ($1.data ?? 0.0) }
          self.ananimusModal = .errorHandler(self.messages)
          //Убираем таимер
          guard let messages = messages, !messages.isEmpty else { return }
          self.managers.logic.timerDissmiss()
        }
      //3 - Проверяем на ошибки
      case .errorHandler(let messages):
        self.VC.ananimusData.messages = messages
        self.ananimusModal = .presentData(self.VC.ananimusData)
      //4 - Презентуем данные
      case .presentData(let ananimusData):
        self.managers.present.messagesTable(messages: ananimusData.messages)
    }
  }
}
//MARK: - Initial
extension AnanimusViewModal {
  
  convenience init(viewController: AnanimusViewController) {
    self.init()
    self.VC       = viewController
    self.managers = AnanimusManagers(viewModal: self)
  }
}
