
import UIKit

extension AnanimusViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  @IBAction func resignFirstResponderGesure(gesture: UITapGestureRecognizer){
    self.viewModal.managers.logic.resignFirstResponder()
  }
  @IBAction func sendMessageButton(button: UIButton){
    self.viewModal.managers.logic.sendMessage()
  }
  @IBAction func addLikeButton(button: UIButton){
    self.viewModal.managers.logic.addLike()
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.viewModal.managers.logic.resignFirstResponder()
  }
}
