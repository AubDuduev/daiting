import UIKit

class AnanimusViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: AnanimusViewModal!
  
  //MARK: - Public variable
  public var ananimusData: AnanimusData!
  
  //MARK: - Outlets
  @IBOutlet weak var sendMessageView      : UIView!
  @IBOutlet weak var sendTextField        : UITextField!
  @IBOutlet weak var ananimusTable        : AnanimusTable!
  @IBOutlet weak var ananimusTableView    : UITableView!
  @IBOutlet weak var sendMessageCornerView: UIView!
  @IBOutlet weak var timerAnsverLabel     : UILabel!
  @IBOutlet weak var timerAnsverView      : UIView!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = AnanimusViewModal(viewController: self)
  }
}
