
import UIKit

class AnanimusTableCell: UITableViewCell, LoadNidoble {
  
 
  private var viewModal: AnanimusViewModal!
  
  @IBOutlet weak var commonView  : UIView!
  @IBOutlet weak var cornerView  : UIView!
  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var timeLabel   : UILabel!
  
  @IBOutlet weak var commonTrailingConstant: NSLayoutConstraint!
  @IBOutlet weak var commonLeadingConstant : NSLayoutConstraint!
  
  public func configure(viewModal: AnanimusViewModal?, message: CODMessage?){
    guard let message = message else { return }
    self.setData(message: message)
  }
  private func setData(message: CODMessage){
  
    let from = GVuserData.ID == message.fromID ? "to": "from"
    guard let recipient = Recipient(rawValue: from) else { return }
    
    switch recipient {
      case .from:
        self.commonTrailingConstant.constant = 65
        self.commonLeadingConstant.constant  = 15
      case .to:
        self.commonTrailingConstant.constant = 15
        self.commonLeadingConstant.constant  = 65
    }
    self.messageLabel.text = message.text
    self.timeLabel.text    = message.data?.format(.time)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.commonView.cornerRadius(4, false)
    self.commonView.shadowColor(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5), radius: 4)
    self.cornerView.cornerRadius(4, true)
  }
  override func prepareForReuse() {
    super.prepareForReuse()
    self.messageLabel.text = nil
    self.timeLabel.text    = nil
  }
  enum Recipient: String {
    case to
    case from
  }
}
