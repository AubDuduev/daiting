
import Foundation
import Protocols

class AnanimusManagers: VMManagers {
  
  let setup    : SetupAnanimus!
  let server   : ServerAnanimus!
  let present  : PresentAnanimus!
  let logic    : LogicAnanimus!
  let animation: AnimationAnanimus!
  let router   : RouterAnanimus!
  
  init(viewModal: AnanimusViewModal) {
    
    self.setup     = SetupAnanimus(viewModal: viewModal)
    self.server    = ServerAnanimus(viewModal: viewModal)
    self.present   = PresentAnanimus(viewModal: viewModal)
    self.logic     = LogicAnanimus(viewModal: viewModal)
    self.animation = AnimationAnanimus(viewModal: viewModal)
    self.router    = RouterAnanimus(viewModal: viewModal)
  }
}

