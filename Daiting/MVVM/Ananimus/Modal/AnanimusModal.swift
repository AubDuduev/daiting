
import UIKit

enum AnanimusModal {
	
  case loading
  case getData(GDCurrentChat)
  case errorHandler([CODMessage]?)
  case presentData(AnanimusData)
}

