
import UIKit
import Protocols

class MainNavBarViewController: UINavigationController {
	
	//MARK: - ViewModel
	public var viewModal: MainNavBarViewModal!
	
	//MARK: - Public variable
	public let navBarView = NavBarView().loadNib()
	
	//MARK: - Outlets
	
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
    self.viewModal.viewDidLoad()
	}
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
	private func initViewModal(){
		self.viewModal = MainNavBarViewModal(navBarViewController: self)
	}
}
