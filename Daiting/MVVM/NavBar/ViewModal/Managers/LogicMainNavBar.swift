import UIKit
import Protocols

class LogicMainNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainNavBarViewModal!
  
  //MARK: - Create View Controllers For TabBar
 
}
//MARK: - Initial
extension LogicMainNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainNavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}
