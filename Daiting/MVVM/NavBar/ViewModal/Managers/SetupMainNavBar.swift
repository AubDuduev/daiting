
import UIKit
import Protocols

class SetupMainNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainNavBarViewModal!
  
}
//MARK: - Private functions
extension SetupMainNavBar {
  
  public func navBarView(){
    let heightNavBar = 55 + self.VM.navBarVC.view.safeAreaInsets.top
    self.VM.navBarVC.navBarView.translatesAutoresizingMaskIntoConstraints = false
    self.VM.navBarVC.navBarView.heightAnchor.constraint(equalToConstant: heightNavBar).isActive = true
    self.VM.navBarVC.navBarView.leadingAnchor.constraint(equalTo: self.VM.navBarVC.view.leadingAnchor).isActive = true
    self.VM.navBarVC.navBarView.trailingAnchor.constraint(equalTo: self.VM.navBarVC.view.trailingAnchor).isActive = true
    self.VM.navBarVC.navBarView.topAnchor.constraint(equalTo: self.VM.navBarVC.view.topAnchor).isActive = true
    self.VM.navBarVC.navBarView.setup()
  }
  public func addedAllView(){
    self.VM.navBarVC.view.addSubview(self.VM.navBarVC.navBarView)
  }
}
//MARK: - Initial
extension SetupMainNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainNavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}


