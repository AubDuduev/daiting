import UIKit
import Protocols

class AnimationMainNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainNavBarViewModal!
  
  
}
//MARK: - Initial
extension AnimationMainNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainNavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

