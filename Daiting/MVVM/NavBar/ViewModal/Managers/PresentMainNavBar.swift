
import UIKit
import Protocols

class PresentMainNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainNavBarViewModal!
  
  
}
//MARK: - Initial
extension PresentMainNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainNavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

