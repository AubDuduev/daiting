import UIKit
import Protocols

class ServerMainNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainNavBarViewModal!
  
  
}
//MARK: - Initial
extension ServerMainNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainNavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}


