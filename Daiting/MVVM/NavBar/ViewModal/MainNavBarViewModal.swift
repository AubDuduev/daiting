
import Foundation
import Protocols

class MainNavBarViewModal: VMManagers {
	
	public var mainNavBarModal: MainNavBarModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: MainNavBarManagers!
  public var navBarVC: MainNavBarViewController!
  
  public func viewDidLoad(){
    self.managers.setup.addedAllView()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.navBarView()
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension MainNavBarViewModal {
  
  convenience init(navBarViewController: MainNavBarViewController!) {
    self.init()
    self.navBarVC = navBarViewController
    self.managers = MainNavBarManagers(viewModal: self)
  }
}
