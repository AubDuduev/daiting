
import Foundation
import Protocols

class MainNavBarManagers: VMManagers {
  
  let setup    : SetupMainNavBar!
  let server   : ServerMainNavBar!
  let present  : PresentMainNavBar!
  let logic    : LogicMainNavBar!
  let animation: AnimationMainNavBar!
  let router   : RouterMainNavBar!
  
  init(viewModal: MainNavBarViewModal) {
    
    self.setup     = SetupMainNavBar(viewModal: viewModal)
    self.server    = ServerMainNavBar(viewModal: viewModal)
    self.present   = PresentMainNavBar(viewModal: viewModal)
    self.logic     = LogicMainNavBar(viewModal: viewModal)
    self.animation = AnimationMainNavBar(viewModal: viewModal)
    self.router    = RouterMainNavBar(viewModal: viewModal)
  }
}

