
import UIKit

enum MainNavBarModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)
	
	struct Data {
	
	}
}

