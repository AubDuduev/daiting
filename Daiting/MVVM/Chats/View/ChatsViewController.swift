import UIKit

class ChatsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: ChatsViewModal!
  
  //MARK: - Public variable
  public var chatsData = ChatsData()
  
  
  //MARK: - Outlets
  @IBOutlet weak var sendMessageView      : UIView!
  @IBOutlet weak var messagesView         : UIView!
  @IBOutlet weak var chatsCollection      : ChatsCollection!
  @IBOutlet weak var chatsCollectionView  : UICollectionView!
  @IBOutlet weak var sendTextField        : UITextField!
  @IBOutlet weak var messagesTable        : MessagesTable!
  @IBOutlet weak var messagesTableView    : UITableView!
  @IBOutlet weak var sendMessageCornerView: UIView!
  @IBOutlet weak var noMessageView        : UIView!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = ChatsViewModal(viewController: self)
  }
}
