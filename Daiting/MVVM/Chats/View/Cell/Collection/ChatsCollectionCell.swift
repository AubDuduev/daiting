
import UIKit

class ChatsCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: ChatsViewModal!
  
  public var anket    : CODAnket!
  public var indexPath: IndexPath!
  
  @IBOutlet weak var avatarView     : UIView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel      : UILabel!
  @IBOutlet weak var photoBlureView : UIView!
  
  public func configure(viewModal: ChatsViewModal?, anket: CODAnket?, indexPath: IndexPath){
    self.viewModal = viewModal
    self.anket     = anket
    self.indexPath = indexPath
    self.viewModal.managers.present.image(cell: self)
    self.viewModal.managers.present.data(cell: self)
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.viewModal?.managers.setup.avatarView(cell: self)
  }
  
  @IBAction func getMessagesButton(button: UIButton){
    self.viewModal.managers.logic.chengeUserMessage(cell: self)
  }
}
