
import UIKit

class ChatsCollection : NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModal     : ChatsViewModal!
  public var ankets        : [CODAnket]!
}

//MARK: - Delegate CollectionView
extension ChatsCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
}

//MARK: - DataSource CollectionView
extension ChatsCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
  return self.ankets?.count ?? 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = ChatsCollectionCell().collectionCell(collectionView, indexPath: indexPath)
    cell.configure(viewModal: viewModal, anket: self.ankets?[indexPath.row], indexPath: indexPath)
    return cell
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension ChatsCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width : CGFloat = 80
    let height: CGFloat = collectionView.frame.height - 20
    return .init(width: width, height: height)
  }
  
}


