
import UIKit

extension ChatsViewController {
  

  @IBAction func resignFirstResponderGesure(gesture: UITapGestureRecognizer){
    self.viewModal.managers.logic.resignFirstResponder()
  }
  @IBAction func sendMessageButton(button: UIButton){
    self.viewModal.managers.logic.sendMessage()
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.viewModal.managers.logic.resignFirstResponder()
  }
}
