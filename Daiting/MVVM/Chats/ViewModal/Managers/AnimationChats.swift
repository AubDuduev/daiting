import UIKit
import Protocols

class AnimationChats: VMManager {
  
  //MARK: - Public variable
  public var VM: ChatsViewModal!
  
  @objc
  public func animationSendMessageView(notification: Notification?, show: Bool){
    guard let userInfo = notification?.userInfo else { return }
    let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
    UIView.animate(withDuration: 0.1) {
      if show {
        var difference = self.VM.VC.sendMessageView.frame.maxY - keyboardFrame.height
        difference = difference + self.VM.VC.sendMessageView.frame.height
        self.VM.VC.sendMessageView.transform = CGAffineTransform(translationX: 0, y: -difference)
      } else {
        self.VM.VC.sendMessageView.transform = .identity
      }
      self.VM.VC.view.layoutIfNeeded()
    }
  }
}
//MARK: - Initial
extension AnimationChats {
  
  //MARK: - Inition
  convenience init(viewModal: ChatsViewModal) {
    self.init()
    self.VM = viewModal
  }
}

