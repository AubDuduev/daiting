
import UIKit
import Protocols

class SetupChats: VMManager {
  
  //MARK: - Public variable
  public var VM: ChatsViewModal!
  
  //MARK: - ChatsViewController
  public func chatsCollection(){
    let collectionViewLayout = ChatsCollectionViewLaytout()
    collectionViewLayout.sectionInset = .init(top: 20, left: 0, bottom: 20, right: 0)
    collectionViewLayout.sectionInsetReference   = .fromContentInset
    collectionViewLayout.scrollDirection         = .horizontal
    collectionViewLayout.minimumLineSpacing      = 0
    collectionViewLayout.minimumInteritemSpacing = 0
    self.VM.VC.chatsCollectionView.collectionViewLayout = collectionViewLayout
    self.VM.VC.chatsCollection.collectionView           = self.VM.VC.chatsCollectionView
    self.VM.VC.chatsCollection.viewModal                = self.VM
    self.VM.VC.chatsCollectionView.reloadData()
  }
  public func messagesView(){
    self.VM.VC.messagesView.cornerRadius(25, true)
  }
  public func addNotification(){
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.animation.animationSendMessageView(notification: notification, show: true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.animation.animationSendMessageView(notification: notification, show: false)
    }
  }
  public func messagesTable(){
    self.VM.VC.messagesTable.viewModal = self.VM
    self.VM.VC.messagesTableView.isUserInteractionEnabled = true
  }
  public func sendMessageCornerView(){
    self.VM.VC.sendMessageCornerView.cornerRadius(5, true)
  }
  //MARK: - ChatsCollectionCell
  public func avatarView(cell: ChatsCollectionCell){
    let corner = cell.avatarView.frame.width / 2
    cell.avatarView.cornerRadius(corner, true)
    let borderColor: UIColor = (self.VM.indexPathChange.row == cell.indexPath.row) ? .red : .white
    cell.avatarView.borderColor(borderColor, 2)
  }
}
//MARK: - Initial
extension SetupChats {
  
  //MARK: - Inition
  convenience init(viewModal: ChatsViewModal) {
    self.init()
    self.VM = viewModal
  }
}


