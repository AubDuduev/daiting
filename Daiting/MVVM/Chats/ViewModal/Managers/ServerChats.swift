import UIKit
import Protocols

class ServerChats: VMManager {
  
  //MARK: - Public variable
  public var VM: ChatsViewModal!
  
  public func getFromMessages(data: GDCurrentChat, completion: @escaping Clousure<[CODMessage]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETFromMessages, data: data) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChats ->, function: getFromMessages -> data: [CODMessage]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getFromMessages(data: data, completion: completion)
          }
        //Susses
        case .object(let object):
          let messages = object as? [CODMessage]
          completion(messages)
          print("Succesful data: class: ServerChats ->, function: getFromMessages ->, data: [CODMessage]?")
          
      }
    }
  }
  public func getToMessages(data: GDCurrentChat, completion: @escaping Clousure<[CODMessage]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETToMessages, data: data) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChats ->, function: getToMessages -> data: [CODMessage]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getToMessages(data: data, completion: completion)
          }
        //Susses
        case .object(let object):
          let messages = object as? [CODMessage]
          completion(messages)
          print("Succesful data: class: ServerChats ->, function: getToMessages ->, data: [CODMessage]?")
          
      }
    }
  }
  public func postMessage(data: GDSendMessages){
    //Request
    self.VM.serverFB.request(requestType: .POSTMessage, data: data) { (serverResult) in
    }
  }
  public func postNewChat(data: GDSendMessages){
    
    //Request
    self.VM.serverFB.request(requestType: .POSTNewChat, data: data) { (serverResult) in
    
    }
  }
  public func getChats(completion: @escaping Clousure<[CODChat]?>){
    guard let userData = GVuserData else { return }
    //Request
    self.VM.serverFB.request(requestType: .GETChats, data: userData) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChats ->, function: getChats -> data: [DECChat] ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getChats(completion: completion)
          }
        //Susses
        case .object(let object):
          let chats = object as? [CODChat]
          completion(chats)
          print("Succesful data: class: ServerChats ->, getChats: getChats ->, data: [DECChat]")
        
      }
    }
  }
  public func getAnkets(completion: @escaping Clousure<[CODAnket]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETAnkets) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChoosePair ->, function: getAnkets -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getAnkets(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerChoosePair ->, function: getAnkets ->, data: [CODAnket]?")
          
      }
    }
  }
}
//MARK: - Initial
extension ServerChats {
  
  //MARK: - Inition
  convenience init(viewModal: ChatsViewModal) {
    self.init()
    self.VM = viewModal
  }
}


