
import UIKit
import SDWebImage
import Protocols

class PresentChats: VMManager {
  
  //MARK: - Public variable
  public var VM: ChatsViewModal!

  
  public func messagesTable(messages: [CODMessage]?){
    self.VM.VC.messagesTable.messages = messages
    self.VM.VC.messagesTable.tableView.reloadData()
    self.VM.VC.messagesTable.tableView.scrollTo(.bottom)
  }
  public func chatCollection(ankets: [CODAnket]?){
    self.VM.VC.chatsCollection.ankets = ankets
    self.VM.VC.chatsCollectionView?.reloadData()
  }
  //MARK: - ChatsCollectionCell
  public func image(cell: ChatsCollectionCell){
    cell.avatarImageView.isSkeletonable = true
    cell.avatarImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .white))
    guard let string = cell.anket?.avatar else { return }
    guard let url    = URL(string: string) else { return }
    cell.avatarImageView.sd_setImage(with: url) { [weak cell] (_, error, _, url) in
      guard let cell = cell else { return }
      if let error = error {
        print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
      } else {
        cell.avatarImageView.hideSkeleton()
      }
    }
  }
  public func data(cell: ChatsCollectionCell){
    cell.nameLabel.isSkeletonable = true
    cell.nameLabel.skeletonCornerRadius = 5
    cell.nameLabel.cornerRadius(5, true)
    cell.nameLabel.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .white))
    guard let name = cell.anket?.name else { return }
    cell.nameLabel.text = name
    cell.nameLabel.hideSkeleton()
  }
}
//MARK: - Initial
extension PresentChats {
  
  //MARK: - Inition
  convenience init(viewModal: ChatsViewModal) {
    self.init()
    self.VM = viewModal
  }
}

