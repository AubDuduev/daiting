import Foundation
import Protocols
import UIKit

class LogicChats: VMManager {
  
  //MARK: - Public variable
  public var VM: ChatsViewModal!
  
  public func getChats(){
    self.VM.chatsModal = .loading
  }
  public func getMessages(){
    guard let currentChat = self.VM.currentChat else { return }
    self.VM.messages.removeAll()
    self.VM.managers.present.messagesTable(messages: nil)
    self.VM.messagesModal = .getData(currentChat)
  }
  public func chengeUserMessage(cell: ChatsCollectionCell){
    self.VM.indexPathChange = cell.indexPath
    self.setCurrentChat(anket: cell.anket)
    self.getMessages()
    self.VM.VC.chatsCollectionView.visibleCells.forEach({ $0.layoutSubviews()})
  }
  public func setCurrentChat(anket: CODAnket?){
    guard let anket = anket else { return }
    self.VM.currentChat = GDCurrentChat(fromUserData: GVuserData, toUserData: anket)
  }
  public func sendMessage(){
    let text    = self.VM.VC.sendTextField.text
    let message = CODMessage(text  : text,
                             data  : Date().timeIntervalSince1970,
                             fromID: self.VM.currentChat.fromUserData.ID)
    let sendMessage = GDSendMessages(fromUserData: self.VM.currentChat.fromUserData,
                                     toUserData  : self.VM.currentChat.toUserData,
                                     message     : message)
    self.VM.managers.server.postMessage(data: sendMessage)
    self.VM.managers.server.postNewChat(data: sendMessage)
    self.VM.VC.sendTextField.text?.removeAll()
  }
  public func resignFirstResponder(){
    self.VM.VC.sendTextField.resignFirstResponder()
  }
  public func noMessageView(show: Bool){
    if show {
      self.VM.VC.noMessageView.isHidden = true
    } else {
      self.VM.VC.noMessageView.isHidden = false
    }
  }
}

//MARK: - Initial
extension LogicChats {
  
  //MARK: - Inition
  convenience init(viewModal: ChatsViewModal) {
    self.init()
    self.VM = viewModal
  }
}
