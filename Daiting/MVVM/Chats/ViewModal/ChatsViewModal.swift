
import Foundation
import Protocols

class ChatsViewModal: VMManagers {
  
  public var chatsModal: ChatsModal = .loading {
    didSet{
      self.getChatsLogic()
    }
  }
  public var messagesModal: MessagesModal = .loading {
    didSet{
      self.getMessagesLogic()
    }
  }
  
  //MARK: - Public variable
  public var managers           : ChatsManagers!
  public var VC                 : ChatsViewController!
  public var serverFB           = ServerFB()
  public var messages           = [CODMessage]()
  public var codChats           = [CODChat]()
  public var ankets             = [CODAnket]()
  public var errorHandlerAnkets = ErrorHandlerAnkets()
  public var indexPathChange    = IndexPath(row: 0, section: 0)
  public var currentChat        : GDCurrentChat!
  
  public func viewDidLoad() {
    self.managers.setup.chatsCollection()
    self.managers.setup.addNotification()
    self.managers.setup.messagesTable()
    self.managers.logic.getChats()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.messagesView()
    self.managers.setup.sendMessageCornerView()
  }
  public func getMessagesLogic(){
    
    switch self.messagesModal {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData(let currentChat):
        
        //Полчуаем исходящие сообщания
        self.managers.server.getFromMessages(data: currentChat) { (messages) in
          
          var toMessages = self.messages.filter{ $0.fromID != currentChat.fromUserData.ID}
          toMessages.append(contentsOf: messages!)
          self.messages = toMessages.sorted{ ($0.data ?? 0.0) < ($1.data ?? 0.0) }
          self.messagesModal = .errorHandler(self.messages)
        }
        //Полчуаем входящие сообщания 
        self.managers.server.getToMessages(data: currentChat) { (messages) in
          
          var fromMessages = self.messages.filter{ $0.fromID != currentChat.toUserData.ID}
          fromMessages.append(contentsOf: messages!)
          self.messages = fromMessages.sorted{ ($0.data ?? 0.0) < ($1.data ?? 0.0) }
          self.messagesModal = .errorHandler(self.messages)
        }
      //3 - Проверяем на ошибки
      case .errorHandler(let messages):
        let messagesData = MessagesData(messages: messages)
        self.messagesModal = .presentData(messagesData)
      //4 - Презентуем данные
      case .presentData(let messagesData):
        self.managers.present.messagesTable(messages: messagesData.messages)
    }
  }
  public func getChatsLogic(){
    
    switch self.chatsModal {
      //1 - Загрузка
      case .loading:
        self.chatsModal = .getData
        
      //2 - Получаем данные
      case .getData:
        //1 - Получаем ид всех пользователей с которыми общался пользователь
        self.managers.server.getChats { (chats) in
          
          self.codChats.removeAll()
          
          //Добовляем все чаты если они есть
          if let chats = chats {
            self.codChats.append(contentsOf: chats)
          }
          //Проверяем есть ли новый чат
          if  let newChat = self.VC.chatsData.newChat {
            //Проверяем есть ли в полученый чатах новый чат
            if let _ = self.codChats.filter({ $0.userID == newChat.userID}).first {
              
            } else {
            //Добовляем новый чат если он есть
            self.codChats.insert(newChat, at: 0)
            }
          }
          if self.codChats.isEmpty {
            self.managers.logic.noMessageView(show: false)
          }
          //1 - Получаем анкеты
          self.managers.server.getAnkets { (ankets) in
            
            //2 - Проверяем на ошибки
            guard self.errorHandlerAnkets.check(ankets: ankets) else { return }
            
            //3 - Презентуем данные
            self.ankets.removeAll()
            self.codChats.forEach { chat in
              if let ankets = ankets?.filter({ $0.ID == chat.userID}), !ankets.isEmpty {
                self.ankets.append(contentsOf: ankets)
                self.chatsModal = .errorHandler(self.ankets)
              }
            }
          }
        }
        
      //3 - Проверяем на ошибки
      case .errorHandler(let ankets):
        guard self.errorHandlerAnkets.check(ankets: ankets) else { return }
        self.VC.chatsData.ankets = ankets
        
//        if let newAnketChat = self.VC.chatsData.newAnket, let index = self.VC.chatsData.ankets!.firstIndex(of: newAnketChat) {
//          self.VC.chatsData.ankets?.remove(at: index)
//          self.VC.chatsData.ankets?.insert(newAnketChat, at: 0)
//
//        } else if let newAnket = self.VC.chatsData.newAnket {
//          self.VC.chatsData.ankets?.insert(newAnket, at: 0)
//        }
        self.managers.logic.noMessageView(show: true)
        self.chatsModal = .presentData(self.VC.chatsData)
        
      //4 - Презентуем данные
      case .presentData(let chatsData):
        self.managers.present.chatCollection(ankets: chatsData.ankets)
        self.managers.logic.setCurrentChat(anket: chatsData.ankets?.first)
        self.managers.logic.getMessages()
    }
  }
}
//MARK: - Initial
extension ChatsViewModal {
  
  convenience init(viewController: ChatsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = ChatsManagers(viewModal: self)
  }
}
