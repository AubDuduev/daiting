
import Foundation

struct ChatsData {
  
  public var newAnket: CODAnket?
  public var newChat : CODChat?
  public var ankets  : [CODAnket]?
  
  init(newAnket: CODAnket? = nil, ankets: [CODAnket]? = nil) {
   
    self.newAnket = newAnket
    self.ankets   = ankets
    self.newChat  = CODChat(userID: newAnket?.ID)
  }
}

enum ChatsDataType {
  case present
  case perfom
}
