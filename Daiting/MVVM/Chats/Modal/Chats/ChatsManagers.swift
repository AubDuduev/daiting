
import Foundation
import Protocols

class ChatsManagers: VMManagers {
  
  let setup    : SetupChats!
  let server   : ServerChats!
  let present  : PresentChats!
  let logic    : LogicChats!
  let animation: AnimationChats!
  let router   : RouterChats!
  
  init(viewModal: ChatsViewModal) {
    
    self.setup     = SetupChats(viewModal: viewModal)
    self.server    = ServerChats(viewModal: viewModal)
    self.present   = PresentChats(viewModal: viewModal)
    self.logic     = LogicChats(viewModal: viewModal)
    self.animation = AnimationChats(viewModal: viewModal)
    self.router    = RouterChats(viewModal: viewModal)
  }
}

