
import UIKit

enum ChatsModal {
	
	case loading
	case getData
	case errorHandler([CODAnket]?)
	case presentData(ChatsData)
	
	
}

