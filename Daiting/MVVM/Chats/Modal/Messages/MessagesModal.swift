
import UIKit

enum MessagesModal {
	
	case loading
	case getData(GDCurrentChat)
	case errorHandler([CODMessage]?)
	case presentData(MessagesData)
	
	
}

