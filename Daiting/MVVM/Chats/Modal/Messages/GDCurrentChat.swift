
import Foundation

struct GDCurrentChat {
  
  let fromUserData: CODUserData!
  let toUserData  : CODAnket!
  
  init(fromUserData: CODUserData, toUserData: CODAnket) {
    self.fromUserData = fromUserData
    self.toUserData   = toUserData
  }
}
