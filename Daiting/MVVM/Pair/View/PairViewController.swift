import UIKit
import Koloda

class PairViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: PairViewModal!
  
  //MARK: - Public variable
  public var kolodaView = GDKolodaView().loadNib()
  
  //MARK: - Outlets
  @IBOutlet weak var pairInfoView: UIView!
  @IBOutlet weak var topView     : UIView!
  @IBOutlet weak var commonView  : UIView!
  @IBOutlet weak var kolodaPair  : KolodaPair!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  private func initViewModal(){
    self.viewModal = PairViewModal(viewController: self)
  }
}
