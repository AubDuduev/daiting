
import UIKit
import Koloda

class GDKolodaView: KolodaView {

  override func frameForCard(at index: Int) -> CGRect {
    let pading: CGFloat = 70
    let width : CGFloat = self.bounds.width  - pading
    let height: CGFloat = self.bounds.height - pading
    let originalX = pading / 2
    let originalY = pading / 2
  return CGRect(x: originalX, y: originalY, width: width, height: height)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
   
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    
  }
}
