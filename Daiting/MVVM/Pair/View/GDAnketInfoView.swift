
import UIKit

class GDAnketInfoView: UIView {

  @IBOutlet weak var commonView         : UIView!
  @IBOutlet weak var likeView           : UIView!
  @IBOutlet weak var likeButton         : UIButton!
  @IBOutlet weak var emailView          : UIView!
  @IBOutlet weak var favoriteView       : UIView!
  @IBOutlet weak var infoView           : UIView!
  @IBOutlet weak var premiumView        : UIView!
  @IBOutlet weak var anketPhotoImageView: UIImageView!
  @IBOutlet weak var anketNameAgeLabel  : UILabel!
  @IBOutlet weak var anketCityLabel     : UILabel!
  @IBOutlet weak var photoBlureView     : UIView!
  
  private let serverFB = ServerFB()
  
  public var viewModal: PairViewModal!
  public var anket    : CODAnket!
  
  public func frameSetup(viewModal: PairViewModal, anket: CODAnket){
    self.viewModal = viewModal
    self.anket     = anket
    
    self.commonView.cornerRadius(20, true)
    self.cornerRadius(20, false)
    self.shadowColor(color: #colorLiteral(red: 0.1176470588, green: 0, blue: 0, alpha: 0.3), radius: 25)
    self.photoBlureView.isHidden = true
    
    //Setup
    self.viewModal.managers.setup.actionsCornerRadius(view: self)
    self.viewModal.managers.setup.actionsGradient(view: self)
    self.viewModal.managers.setup.infoView(view: self)
    self.viewModal.managers.setup.premiumView(view: self)
    
    //Present
    self.viewModal.managers.present.anketPhoto(view: self)
    self.viewModal.managers.present.anketNameAge(view: self)
    self.viewModal.managers.logic.testLikes(view: self)
    
  }
  public func postInvitation(){
    let data = GDInvitation(selfUser  : GVuserData,
                            toUserData: self.anket)
    //Request
    self.serverFB.request(requestType: .POSTInvitation, data: data) { (serverResult) in
      
    }
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.viewModal?.managers.setup.actionsCornerRadius(view: self)
    self.viewModal.managers.setup.premiumView(view: self)
  }
  
  
  @IBAction func setLikeButton(button: UIButton){
    self.viewModal.managers.logic.setLike(view: self)
  }
  @IBAction func chatButton(button: UIButton){
    self.viewModal.managers.logic.pushChatsVC(anket: self.anket)
  }
  @IBAction func privateChatButton(button: UIButton){
    self.postInvitation()
    self.viewModal.managers.logic.pushPrivateChatsVC(anket: self.anket)
  }
  override init(frame: CGRect) {
    super.init(frame: frame)
    
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    
  }
}
