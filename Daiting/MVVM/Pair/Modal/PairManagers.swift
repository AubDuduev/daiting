
import Foundation
import Protocols

class PairManagers: VMManagers {
  
  let setup    : SetupPair!
  let server   : ServerPair!
  let present  : PresentPair!
  let logic    : LogicPair!
  let animation: AnimationPair!
  let router   : RouterPair!
  
  init(viewModal: PairViewModal) {
    
    self.setup     = SetupPair(viewModal: viewModal)
    self.server    = ServerPair(viewModal: viewModal)
    self.present   = PresentPair(viewModal: viewModal)
    self.logic     = LogicPair(viewModal: viewModal)
    self.animation = AnimationPair(viewModal: viewModal)
    self.router    = RouterPair(viewModal: viewModal)
  }
}

