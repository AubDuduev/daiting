
import UIKit

enum PairModal {
	
	case loading
	case getData
	case errorHandler([CODAnket]?)
	case presentData(PairData)
  case setup
}

