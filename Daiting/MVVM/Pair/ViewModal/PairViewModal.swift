
import Foundation
import Protocols
import AnimatedGradientView

class PairViewModal: VMManagers {
	
	public var pairModal: PairModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers            : PairManagers!
  public var VC                  : PairViewController!
  public var gradientKolodaView  = AnimatedGradientView()
  public var serverFB            = ServerFB()
  public var errorHandlerAnkets  = ErrorHandlerAnkets()
  public var allLikes            = [CODAnket]()
  public var currentAnketInfoView: GDAnketInfoView!
  
  public func viewDidLoad() {
    self.managers.setup.addedView()
    self.managers.setup.kolodaView()
    self.managers.setup.gradientKolodaView()
    self.pairModal = .loading
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.pairInfoViewFrame()
    self.managers.setup.kolodaViewFrame()
  }
  public func viewDidAppear() {
    self.pairModal = .loading
  }
  public func commonLogic(){
    
    switch self.pairModal {
      //1 - Загрузка
      case .loading:
        
        self.pairModal = .getData
        
      //2 - Получаем данные
      case .getData:
        self.managers.server.getAnkets { [weak self] (ankets) in
          guard let self = self else { return }
          self.pairModal = .errorHandler(ankets)
          
        }
      //3 - Проверяем на ошибки
      case .errorHandler(let ankets):
        guard self.errorHandlerAnkets.check(ankets: ankets) else { return }
        let pairData = PairData(ankets: ankets!)
        self.pairModal = .presentData(pairData)
        
      //4 - Презентуем данные
      case .presentData(let pairData):
        self.managers.present.kolodaView(pairData: pairData)
        self.pairModal = .setup
        
      //5 - Настраеваем отсальные данные
      case .setup:
        self.managers.logic.getMyLikes()
    }
  }
}
//MARK: - Initial
extension PairViewModal {
  
  convenience init(viewController: PairViewController) {
    self.init()
    self.VC       = viewController
    self.managers = PairManagers(viewModal: self)
  }
}
