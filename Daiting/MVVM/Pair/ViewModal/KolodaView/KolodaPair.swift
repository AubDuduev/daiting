
import Koloda
import UIKit

class KolodaPair: NSObject, KolodaViewDataSource {
  
  public var viewModal: PairViewModal!
  public var ankets   : [CODAnket]!
  
  func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
    let kolodaPhotoView = GDAnketInfoView().loadNib()
    kolodaPhotoView.frameSetup(viewModal: self.viewModal, anket: self.ankets[index])
    return kolodaPhotoView
  }
  func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
    print(index, "didShowCardAt")
  }
  func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
    return ankets?.count ?? 0
  }
  
  func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
    return .moderate
  }
  func kolodaSwipeThresholdRatioMargin(_ koloda: KolodaView) -> CGFloat? {
    return 0
  }
}

extension KolodaPair: KolodaViewDelegate {
  
  func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
    koloda.resetCurrentCardIndex()
  }
  
  func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
    
  }
  
  func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
    return false
  }
  
  func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
    return true
  }
  
  func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
    return true
  }
  
}
