
import UIKit
import Protocols
import SnapKit
import AnimatedGradientView

class SetupPair: VMManager {
  
  //MARK: - Public variable
  public var VM: PairViewModal!
  
  public func pairInfoViewFrame(){
    self.VM.VC.pairInfoView.cornerRadius(40, true)
  }
  public func kolodaView(){
    self.VM.VC.kolodaPair.viewModal  = self.VM
    self.VM.VC.kolodaView.delegate   = self.VM.VC.kolodaPair
    self.VM.VC.kolodaView.dataSource = self.VM.VC.kolodaPair
    self.VM.VC.kolodaView.backgroundColor = .clear
  }
  public func kolodaViewFrame(){
    self.VM.VC.kolodaView.snp.makeConstraints { (kolodaView) in
      kolodaView.left.equalTo(self.VM.VC.commonView).inset(0)
      kolodaView.right.equalTo(self.VM.VC.commonView).inset(0)
      kolodaView.top.equalTo(self.VM.VC.commonView).inset(40)
      kolodaView.bottom.equalTo(self.VM.VC.commonView).inset(80)
    }
  }
  public func gradientKolodaView(){
    self.VM.gradientKolodaView.direction  = .upRight
    self.VM.gradientKolodaView.colors     = [[.set(.oneGradient), .set(.twoGradient)]]
    self.VM.gradientKolodaView.autoRepeat = true
    self.VM.gradientKolodaView.startAnimating()
  }
  public func gradientKolodaViewFrame(){
    self.VM.gradientKolodaView.snp.makeConstraints { (kolodaView) in
      kolodaView.left.equalTo(self.VM.VC.kolodaView).inset(0)
      kolodaView.right.equalTo(self.VM.VC.kolodaView).inset(0)
      kolodaView.top.equalTo(self.VM.VC.kolodaView).inset(0)
      kolodaView.bottom.equalTo(self.VM.VC.kolodaView).inset(0)
    }
  }
  public func addedView(){
    self.VM.VC.commonView.addSubview(self.VM.VC.kolodaView)
  }
  //MARK: - GDAnketInfoView
  public func actionsCornerRadius(view: GDAnketInfoView){
    let cornerCenter = view.likeView.frame.height / 2
    let cornerEge    = view.favoriteView.frame.height / 2
    view.likeView.cornerRadius(cornerCenter, false)
    view.favoriteView.cornerRadius(cornerEge, false)
    view.emailView.cornerRadius(cornerEge, false)
    view.likeView.shadowColor(color: #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1), radius: 5)
    view.emailView.shadowColor(color: #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1), radius: 5)
    view.favoriteView.shadowColor(color: #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1), radius: 5)
  }
  public func actionsGradient(view: GDAnketInfoView){
    let gradientLike     = AnimatedGradientView()
    let gradientEmail    = AnimatedGradientView()
    let gradientFavorite = AnimatedGradientView()
    
    gradientLike.direction = .upRight
    gradientLike.colors = [[UIColor.set(.oneGradient), UIColor.set(.twoGradient)]]
    
    gradientEmail.direction = .upRight
    gradientEmail.colors = [[.set(.oneGradient), .set(.twoGradient)]]
    
    gradientFavorite.direction = .upRight
    gradientFavorite.colors = [[.set(.oneGradient), .set(.twoGradient)]]
    
    let cornerCenter = view.likeView.frame.height / 2
    let cornerEge    = view.favoriteView.frame.height / 2
    gradientLike.cornerRadius(cornerCenter, true)
    gradientEmail.cornerRadius(cornerEge, true)
    gradientFavorite.cornerRadius(cornerEge, true)
    
    view.likeView.insertSubview(gradientLike, at: 0)
    view.emailView.insertSubview(gradientEmail, at: 0)
    view.favoriteView.insertSubview(gradientFavorite, at: 0)
    
    gradientLike.snp.makeConstraints { (gradientLike) in
      gradientLike.edges.equalTo(view.likeView)
    }
    gradientEmail.snp.makeConstraints { (gradientEmail) in
      gradientEmail.edges.equalTo(view.emailView)
    }
    gradientFavorite.snp.makeConstraints { (gradientFavorite) in
      gradientFavorite.edges.equalTo(view.favoriteView)
    }
  }
  public func infoView(view: GDAnketInfoView){
    let gradient = AnimatedGradientView()
    gradient.direction = .upRight
    gradient.colors = [[.set(.oneGradient), .set(.twoGradient)]]
    view.infoView.insertSubview(gradient, at: 0)
    gradient.snp.makeConstraints { (gradient) in
      gradient.edges.equalTo(view.infoView)
    }
  }
  public func premiumView(view: GDAnketInfoView){
    view.premiumView.cornerRadius(15, true)
  }
}
//MARK: - Initial
extension SetupPair {
  
  //MARK: - Inition
  convenience init(viewModal: PairViewModal) {
    self.init()
    self.VM = viewModal
  }
}


