import Foundation
import Protocols

class LogicPair: VMManager {
  
  //MARK: - Public variable
  public var VM: PairViewModal!
  
  public func setLike(view: GDAnketInfoView){
    guard GVuserData != nil else {
      AlertEK.dеfault(title: .information, message: .registrRequaer)
      return
    }
    self.VM.currentAnketInfoView = view
    view.anket.isLike = true
    //Смотрм есть ли такая анкета в списке симпатий
    //если есть то удаляем
    if let anket = self.VM.allLikes.filter({$0.ID == view.anket.ID}).first {
      let userAnket  = CODAnket(userData: GVuserData)
      let updateUser = GDUpdateUser(userAnket: userAnket, anket: anket)
      self.VM.managers.server.removeSympathy(updateUser: updateUser)
    //если есть нет то удаляем 
    } else {
      var userAnket  = CODAnket(userData: GVuserData)
      userAnket.isLike = true
      let updateUser = GDUpdateUser(userAnket: userAnket, anket: view.anket)
      self.VM.managers.server.addLike(updateUser: updateUser)
    }
  }
  public func getMyLikes(){
    guard GVuserData != nil else { return }
    //1 - Получаем данные
    self.VM.managers.server.getMyLikes { (likes) in
      //2 - Проверяем на ошибки
      self.VM.allLikes.removeAll()
      guard let likes = likes, !likes.isEmpty else {
        self.testLikes()
        return
      }
      self.VM.allLikes.append(contentsOf: likes)
      self.testLikes()
    }
  }
  public func testLikes(view: GDAnketInfoView? = nil){
    if view != nil {
      self.VM.currentAnketInfoView = view
    }
    if let _ = self.VM.allLikes.filter({$0.ID == self.VM.currentAnketInfoView.anket?.ID}).first {
      self.VM.currentAnketInfoView.likeButton.tintColor = .red
    } else {
      self.VM.currentAnketInfoView.likeButton.tintColor = .white
    }
  }
  public func pushPrivateChatsVC(anket: CODAnket){
    let ananimusData = AnanimusData(anket: anket)
    self.VM.managers.router.push(.AnanimusVC(ananimusData))
  }
  public func pushChatsVC(anket: CODAnket){
    let chatsDat = ChatsData(newAnket: anket)
    self.VM.managers.router.push(.ChatsVC(chatsDat))
  }
}
//MARK: - Initial
extension LogicPair {
  
  //MARK: - Inition
  convenience init(viewModal: PairViewModal) {
    self.init()
    self.VM = viewModal
  }
}
