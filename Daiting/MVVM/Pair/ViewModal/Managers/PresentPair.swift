
import UIKit
import Protocols
import SkeletonView
import SDWebImage

class PresentPair: VMManager {
  
  //MARK: - Public variable
  public var VM: PairViewModal!
  
 
  public func kolodaView(pairData: PairData){
    self.VM.VC.kolodaPair.ankets = pairData.ankets
    self.VM.VC.kolodaView.reloadData()
  }
  //MARK: - GDAnketInfoView
  public func anketPhoto(view: GDAnketInfoView){
    guard let imageURL = view.anket?.avatar else { return }
    guard let url      = URL(string: imageURL) else { return }
    view.anketPhotoImageView.isSkeletonable = true
    view.anketPhotoImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .white))
    view.anketPhotoImageView.sd_setImage(with: url) { (_, error, _, url) in
      
      if let error = error {
        print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
      } else {
        view.anketPhotoImageView.hideSkeleton()
      }
    }
  }
  public func anketNameAge(view: GDAnketInfoView){
    let name = (view.anket?.name ?? "")
    let age  = (view.anket?.age  ?? "")
    let newAge = (age == "") ? "" : ", \(age) Лет"
    view.anketNameAgeLabel.text = "\(name) \(newAge)"
    view.anketCityLabel.text    = view.anket.city
  }
}
//MARK: - Initial
extension PresentPair {
  
  //MARK: - Inition
  convenience init(viewModal: PairViewModal) {
    self.init()
    self.VM = viewModal
  }
}

