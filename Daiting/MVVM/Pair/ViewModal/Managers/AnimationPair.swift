import UIKit
import Protocols

class AnimationPair: VMManager {
  
  //MARK: - Public variable
  public var VM: PairViewModal!
  
  
}
//MARK: - Initial
extension AnimationPair {
  
  //MARK: - Inition
  convenience init(viewModal: PairViewModal) {
    self.init()
    self.VM = viewModal
  }
}

