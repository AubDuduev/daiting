
import UIKit
import Protocols

class RouterPair: VMManager {
  
  //MARK: - Public variable
  public var VM: PairViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .ChatsVC(let chatsData):
        self.pushChatsVC(chatsData: chatsData)
      case .AnanimusVC(let ananimusData):
        self.pushAnanimusVC(ananimusData: ananimusData)
    }
  }
  
  enum Push {
    case ChatsVC(ChatsData)
    case AnanimusVC(AnanimusData)
  }
}
//MARK: - Private functions
extension RouterPair {
  
  private func pushChatsVC(chatsData: ChatsData){
    let tabBarController = (self.VM.VC.tabBarController as? MainTabBarViewController)
    let button = UIButton()
    button.tag = 0
    let chatsVC = (tabBarController?.viewControllers?.first as! ChatsViewController)
    chatsVC.chatsData = chatsData
    tabBarController?.tabBarView.actionButton(button: button)
  }
  private func pushAnanimusVC(ananimusData: AnanimusData){
    let ananimusVC = self.VM.VC.getVCForID(storyboardID     : .Ananimus,
                                           vcID             : .AnanimusVC,
                                           transitionStyle  : .crossDissolve,
                                           presentationStyle: .fullScreen) as! AnanimusViewController
    ananimusVC.ananimusData = ananimusData
    self.VM.VC.present(ananimusVC, animated: true)
  }
}
//MARK: - Initial
extension RouterPair {
  
  //MARK: - Inition
  convenience init(viewModal: PairViewModal) {
    self.init()
    self.VM = viewModal
  }
}



