import UIKit
import Protocols

class ServerPair: VMManager {
  
  //MARK: - Public variable
  public var VM: PairViewModal!
  
  public func getAnkets(completion: @escaping Clousure<[CODAnket]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETAnkets) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerPair ->, function: getAnkets -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getAnkets(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerPair ->, function: getAnkets ->, data: [CODAnket]?")
          
      }
    }
  }
  public func addDelight(updateUser: GDUpdateUser){
    //Request
    self.VM.serverFB.request(requestType: .POSTDelight, data: updateUser) { (serverResult) in
      
    }
  }
  public func addLike(updateUser: GDUpdateUser){
    //Request
    self.VM.serverFB.request(requestType: .POSTSympathy, data: updateUser) { (serverResult) in
      
    }
  }
  public func addBlackList(updateUser: GDUpdateUser){
    //Request
    self.VM.serverFB.request(requestType: .POSTBlackList, data: updateUser) { (serverResult) in
    }
  }
  public func getMyLikes(completion: @escaping Clousure<[CODAnket]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETMySympathys, data: GVuserData) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChoosePair ->, function: getMyLikes -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getMyLikes(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerChoosePair ->, function: getMyLikes ->, data: [CODAnket]?")
          
      }
    }
  }
  public func getMyDelights(completion: @escaping Clousure<[CODAnket]?>){
    //Request
    self.VM.serverFB.request(requestType: .GETMyDelights, data: GVuserData) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerChoosePair ->, function: getAnkets -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getMyDelights(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerChoosePair ->, function: getAnkets ->, data: [CODAnket]?")
          
      }
    }
  }
  public func removeDelight(updateUser: GDUpdateUser){
    //Request
    self.VM.serverFB.request(requestType: .DELETEDelight, data: updateUser) { (serverResult) in
    
    }
  }
  public func removeSympathy(updateUser: GDUpdateUser){
    //Request
    self.VM.serverFB.request(requestType: .DELETESympathy, data: updateUser) { (serverResult) in
    
    }
  }
}
//MARK: - Initial
extension ServerPair {
  
  //MARK: - Inition
  convenience init(viewModal: PairViewModal) {
    self.init()
    self.VM = viewModal
  }
}


