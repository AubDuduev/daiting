
import Foundation
import Protocols

class LikesManagers: VMManagers {
  
  let setup    : SetupLikes!
  let server   : ServerLikes!
  let present  : PresentLikes!
  let logic    : LogicLikes!
  let animation: AnimationLikes!
  let router   : RouterLikes!
  
  init(viewModal: LikesViewModal) {
    
    self.setup     = SetupLikes(viewModal: viewModal)
    self.server    = ServerLikes(viewModal: viewModal)
    self.present   = PresentLikes(viewModal: viewModal)
    self.logic     = LogicLikes(viewModal: viewModal)
    self.animation = AnimationLikes(viewModal: viewModal)
    self.router    = RouterLikes(viewModal: viewModal)
  }
}

