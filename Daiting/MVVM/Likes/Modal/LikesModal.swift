
import UIKit

enum LikesModal {
	
  case loading
  case getData(SympathyType)
  case errorHandler([CODAnket]?, SympathyType)
	case presentData(LikesData, SympathyType)
	
}

enum SympathyType {
  case Outbox
  case Inbox
  
  
}
