
import UIKit

class LikesCollection : NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModal     : LikesViewModal!
  public var ankets        : [CODAnket]!
  public var symphatyType  : SympathyType!
}

//MARK: - Delegate CollectionView
extension LikesCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
}

//MARK: - DataSource CollectionView
extension LikesCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
  return self.ankets?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = LikesCollectionCell().collectionCell(collectionView, indexPath: indexPath)
    cell.configure(viewModal: viewModal, anket: ankets?[indexPath.row], type: self.symphatyType)
    return cell
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension LikesCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width : CGFloat = collectionView.frame.width
    let height: CGFloat = 90
    return .init(width: width, height: height)
  }
  
}


