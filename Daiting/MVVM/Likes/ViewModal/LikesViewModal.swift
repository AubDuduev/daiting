
import Foundation
import Protocols

class LikesViewModal: VMManagers {
	
	public var likesModal: LikesModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers            : LikesManagers!
  public var VC                  : LikesViewController!
  public var errorHandlerAnkets  = ErrorHandlerAnkets()
  public var serverFB            = ServerFB()
  
  public func viewDidLoad() {
    self.managers.setup.segmentControll()
    self.managers.setup.likesCollection()
  }
  public func viewDidAppear() {
    self.likesModal = .loading
  }
  public func commonLogic(){
    
    switch self.likesModal {
      //1 - Загрузка
      case .loading:
        
        self.likesModal = .getData(.Outbox)
        self.managers.logic.noLikesView(show: true)
      //2 - Получаем данные
      case .getData(let sympathyType):
        
        switch sympathyType {
          case .Inbox:
            self.managers.server.getISympathy { [weak self] (ankets) in
              guard let self = self else { return }
              self.likesModal = .errorHandler(ankets, sympathyType)
              
            }
          case .Outbox:
            self.managers.server.getMySympathy { [weak self] (ankets) in
              guard let self = self else { return }
              self.likesModal = .errorHandler(ankets, sympathyType)
              
            }
        }
      //3 - Проверяем на ошибки
      case .errorHandler(let ankets, let sympathyType):
        guard self.errorHandlerAnkets.check(ankets: ankets) else { return }
        let likesData = LikesData(ankets: ankets!)
        self.likesModal = .presentData(likesData, sympathyType)
        
      //4 - Презентуем данные
      case .presentData(let likesData, let sympathyType):
        self.managers.present.collection(likesData: likesData, sympathyType: sympathyType)
        self.managers.logic.noLikesView(show: false)
    }
  }
  
}
//MARK: - Initial
extension LikesViewModal {
  
  convenience init(viewController: LikesViewController) {
    self.init()
    self.VC       = viewController
    self.managers = LikesManagers(viewModal: self)
  }
}
