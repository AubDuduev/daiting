import Foundation
import Protocols
import UIKit

class LogicLikes: VMManager {
  
  //MARK: - Public variable
  public var VM: LikesViewModal!
  
  public func getSymphatys(segment: UISegmentedControl){
    switch segment.selectedSegmentIndex {
      case 0:
        self.VM.managers.present.collection(likesData: nil, sympathyType: .Outbox)
        self.VM.likesModal = .getData(.Outbox)
      case 1:
        self.VM.managers.present.collection(likesData: nil, sympathyType: .Inbox)
        self.VM.likesModal = .getData(.Inbox)
      default:
        break
    }
  }
  public func pushChatsVC(anket: CODAnket){
    let chatsDat = ChatsData(newAnket: anket)
    self.VM.managers.router.push(.ChatsVC(chatsDat))
  }
  public func noLikesView(show: Bool){
    if show {
      self.VM.VC.noLikesView.isHidden = !show
    } else {
      self.VM.VC.noLikesView.isHidden = !show
    }
  }
}
//MARK: - Initial
extension LogicLikes {
  
  //MARK: - Inition
  convenience init(viewModal: LikesViewModal) {
    self.init()
    self.VM = viewModal
  }
}
