import UIKit
import Protocols

class ServerLikes: VMManager {
  
  //MARK: - Public variable
  public var VM: LikesViewModal!
  
  public func getISympathy(completion: @escaping Clousure<[CODAnket]?>){
    guard let userData = GVuserData else { return }
    //Request
    self.VM.serverFB.request(requestType: .GETISympathys, data: userData) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerSympathy ->, function: getISympathy -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getISympathy(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerSympathy ->, function: getISympathy ->, data: [CODAnket]?")
          
      }
    }
  }
  public func getMySympathy(completion: @escaping Clousure<[CODAnket]?>){
    guard let userData = GVuserData else { return }
    //Request
    self.VM.serverFB.request(requestType: .GETMySympathys, data: userData) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerSympathy ->, function: getMySympathy -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getMySympathy(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerSympathy ->, function: getMySympathy ->, data: [CODAnket]?")
          
      }
    }
  }
}
//MARK: - Initial
extension ServerLikes {
  
  //MARK: - Inition
  convenience init(viewModal: LikesViewModal) {
    self.init()
    self.VM = viewModal
  }
}


