
import UIKit
import Protocols

class PresentLikes: VMManager {
  
  //MARK: - Public variable
  public var VM: LikesViewModal!
  
  public func collection(likesData: LikesData?, sympathyType: SympathyType){
    self.VM.VC.likesCollection.ankets       = likesData?.ankets
    self.VM.VC.likesCollection.symphatyType = sympathyType
    self.VM.VC.likesCollectionView.reloadData()
  }
  //MARK: - LikesCollectionCell
  public func photo(cell: LikesCollectionCell){
    cell.anketImageView.isSkeletonable = true
    cell.anketImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .set(.oneGradient)))
    let corner = cell.avatarView.frame.width / 2
    cell.anketImageView.skeletonCornerRadius = Float(corner)
    guard let imageURL = cell.anket?.avatar else { return }
    guard let url      = URL(string: imageURL) else { return }
    cell.anketImageView.sd_setImage(with: url) { [weak cell] (_, error, _, url) in
      guard let cell = cell else { return }
      if let error = error {
        print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
      } else {
        cell.anketImageView.hideSkeleton()
      }
    }
  }
  public func data(cell: LikesCollectionCell){
    let name = (cell.anket?.name ?? "")
    let age  = (cell.anket?.age  ?? "")
    cell.anketNameAgeLabel.text = "\(name), \(age) Лет"
    cell.anketCityLabel.text    = cell.anket?.city
  }
  public func prepareForReuse(cell: LikesCollectionCell){
    cell.anketImageView.image   = nil
    cell.anketNameAgeLabel.text = nil
    cell.anketCityLabel.text    = nil
  }
}
//MARK: - Initial
extension PresentLikes {
  
  //MARK: - Inition
  convenience init(viewModal: LikesViewModal) {
    self.init()
    self.VM = viewModal
  }
}

