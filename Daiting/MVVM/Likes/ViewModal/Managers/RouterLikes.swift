
import UIKit
import Protocols

class RouterLikes: VMManager {
  
  //MARK: - Public variable
  public var VM: LikesViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .ChatsVC(let chatsData):
        self.pushChatsVC(chatsData: chatsData)
    }
  }
  
  enum Push {
    case ChatsVC(ChatsData)
  }
}
//MARK: - Private functions
extension RouterLikes {
  
  private func pushChatsVC(chatsData: ChatsData){
    let tabBarController = (self.VM.VC.tabBarController as? MainTabBarViewController)
    let button = UIButton()
    button.tag = 0
    let chatsVC = (tabBarController?.viewControllers?.first as! ChatsViewController)
    chatsVC.chatsData = chatsData
    tabBarController?.tabBarView.actionButton(button: button)
  }
}
//MARK: - Initial
extension RouterLikes {
  
  //MARK: - Inition
  convenience init(viewModal: LikesViewModal) {
    self.init()
    self.VM = viewModal
  }
}



