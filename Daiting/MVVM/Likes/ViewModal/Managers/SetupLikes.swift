
import UIKit
import Protocols

class SetupLikes: VMManager {
  
  //MARK: - Public variable
  public var VM: LikesViewModal!
  
  //MARK: - LikesViewController
  public func segmentControll(){
    let atributes = [NSAttributedString.Key.foregroundColor : UIColor.white,
                     NSAttributedString.Key.font : UIFont.set(.lemon, 15)]
    self.VM.VC.changeSegmentControl.setTitleTextAttributes(atributes, for: .normal)
  }
  public func likesCollection(){
    let collectionViewLayout = LikesCollectionViewLaytout()
    collectionViewLayout.sectionInset = .init(top: 10, left: 0, bottom: 10, right: 0)
    collectionViewLayout.sectionInsetReference = .fromContentInset
    self.VM.VC.likesCollectionView.collectionViewLayout = collectionViewLayout
    self.VM.VC.likesCollection.collectionView           = self.VM.VC.likesCollectionView
    self.VM.VC.likesCollection.viewModal                = self.VM
    self.VM.VC.likesCollectionView.reloadData()
  }
  //MARK: - LikesCollectionCell
  public func chantView(cell: LikesCollectionCell){
    let corner = cell.chantView.frame.width / 2
    cell.chantView.cornerRadius(corner, false)
    cell.chantView.shadowColor(color: #colorLiteral(red: 0.1176470588, green: 0, blue: 0, alpha: 0.5), radius: 5)
    cell.chantView.borderColor(.set(.twoGradient), 2)
  }
  public func avatarView(cell: LikesCollectionCell){
    let corner = cell.avatarView.frame.width / 2
    cell.avatarView.cornerRadius(corner, true)
  }
  public func photoBlureView(cell: LikesCollectionCell){
    switch cell.symphatyType! {
      case .Inbox:
        cell.photoBlureView.isHidden = true
      case .Outbox:
        cell.photoBlureView.isHidden = false
    }
  }
}
//MARK: - Initial
extension SetupLikes {
  
  //MARK: - Inition
  convenience init(viewModal: LikesViewModal) {
    self.init()
    self.VM = viewModal
  }
}


