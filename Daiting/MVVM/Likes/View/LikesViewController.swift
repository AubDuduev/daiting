import UIKit

class LikesViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: LikesViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet weak var changeSegmentControl: UISegmentedControl!
  @IBOutlet weak var likesCollection     : LikesCollection!
  @IBOutlet weak var likesCollectionView : UICollectionView!
  @IBOutlet weak var noLikesView         : UIView!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  private func initViewModal(){
    self.viewModal = LikesViewModal(viewController: self)
  }
}
