
import UIKit

class LikesCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: LikesViewModal!
  
  public var anket       : CODAnket!
  public var symphatyType: SympathyType!
  
  @IBOutlet weak var chantView        : UIView!
  @IBOutlet weak var avatarView       : UIView!
  @IBOutlet weak var anketImageView   : UIImageView!
  @IBOutlet weak var anketNameAgeLabel: UILabel!
  @IBOutlet weak var anketCityLabel   : UILabel!
  @IBOutlet weak var photoBlureView   : UIView!
  
  public func configure(viewModal: LikesViewModal?, anket: CODAnket?, type: SympathyType){
    self.viewModal    = viewModal
    self.anket        = anket
    self.symphatyType = type
    self.viewModal.managers.present.data(cell: self)
    self.viewModal.managers.present.photo(cell: self)
    self.viewModal.managers.setup.photoBlureView(cell: self)
  }
  override func prepareForReuse() {
    super.prepareForReuse()
    self.viewModal.managers.present.prepareForReuse(cell: self)
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.viewModal?.managers.setup.chantView(cell: self)
    self.viewModal?.managers.setup.avatarView(cell: self)
  }
  @IBAction func pushChatsButton(button: UIButton){
    self.viewModal.managers.logic.pushChatsVC(anket: self.anket)
  }
}
