import Protocols
import Foundation

class MainTabBarManagers: VMManagers {
  
  let setup    : SetupMainTabBar!
  let server   : ServerMainTabBar!
  let present  : PresentMainTabBar!
  let logic    : LogicMainTabBar!
  let animation: AnimationMainTabBar!
  let router   : RouterMainTabBar!
  
  init(viewModal: MainTabBarViewModal) {
    
    self.setup     = SetupMainTabBar(viewModal: viewModal)
    self.server    = ServerMainTabBar(viewModal: viewModal)
    self.present   = PresentMainTabBar(viewModal: viewModal)
    self.logic     = LogicMainTabBar(viewModal: viewModal)
    self.animation = AnimationMainTabBar(viewModal: viewModal)
    self.router    = RouterMainTabBar(viewModal: viewModal)
  }
}

