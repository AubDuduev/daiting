
import Foundation
import Protocols
import SwiftEntryKit

class MainTabBarViewModal: VMManagers {
	
	public var mainTabBarModal: MainTabBarModal = .loading {
		didSet {
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers     : MainTabBarManagers!
  public var tabbarVC     : MainTabBarViewController!
  public var attributes   = EKAttributes()
  public let ananimusView = AnanimusView().loadNib()
  public let serverFB     = ServerFB()
  
  public func viewDidLoad() {
    self.managers.server.deleteInvantion()
    self.managers.setup.tabBar()
    self.managers.setup.setupTabbarVC()
    self.managers.setup.ananimusAttributesView()
    self.managers.setup.addedAllView()
    self.managers.setup.setupTabbarVC()
    self.managers.logic.tapButton()
    self.managers.logic.aceptClousureAnanimusView()
  }
  public func viewDidAppear() {
    self.managers.logic.subscribe()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.tabBarView()
  }
  public func commonLogic(){
    
    //1 - Получаем данные
   
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension MainTabBarViewModal {
  
  convenience init(tabBarViewController: MainTabBarViewController!) {
    self.init()
    self.tabbarVC = tabBarViewController
    self.managers = MainTabBarManagers(viewModal: self)
  }
}
