import Protocols
import UIKit

class PresentMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModal!
  
  
}
//MARK: - Initial
extension PresentMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainTabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

