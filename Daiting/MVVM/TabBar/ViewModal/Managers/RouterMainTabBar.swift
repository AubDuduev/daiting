import Protocols
import UIKit

class RouterMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .AnanimusVC(let ananimusData):
        self.pushAnanimusVC(ananimusData: ananimusData)
    }
  }
  
  enum Push {
    case AnanimusVC(AnanimusData)
  }
}
//MARK: - Private functions
extension RouterMainTabBar {
  
  private func pushAnanimusVC(ananimusData: AnanimusData){
    let ananimusVC = self.VM.tabbarVC.getVCForID(storyboardID     : .Ananimus,
                                                 vcID             : .AnanimusVC,
                                                 transitionStyle  : .crossDissolve,
                                                 presentationStyle: .fullScreen) as! AnanimusViewController
    ananimusVC.ananimusData = ananimusData
    self.VM.tabbarVC.present(ananimusVC, animated: true)
  }
}
//MARK: - Initial
extension RouterMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainTabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}



