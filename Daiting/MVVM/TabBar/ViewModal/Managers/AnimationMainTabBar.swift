import UIKit
import Protocols

class AnimationMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModal!
  
  
}
//MARK: - Initial
extension AnimationMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainTabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

