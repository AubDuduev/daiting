import Protocols
import UIKit
import SwiftEntryKit

class SetupMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModal!
  
}
//MARK: - Private functions
extension SetupMainTabBar {
  
  public func setupTabbarVC(){
    self.VM.tabbarVC.setViewControllers(self.VM.managers.logic.createVcForTabBar(), animated: true)
  }
  public func tabBarView(){
    let heightTabBar = 55 + self.VM.tabbarVC.view.safeAreaInsets.bottom
    self.VM.tabbarVC.tabBarView.translatesAutoresizingMaskIntoConstraints = false
    self.VM.tabbarVC.tabBarView.heightAnchor.constraint(equalToConstant: heightTabBar).isActive = true
    self.VM.tabbarVC.tabBarView.leadingAnchor.constraint(equalTo: self.VM.tabbarVC.view.leadingAnchor).isActive = true
    self.VM.tabbarVC.tabBarView.trailingAnchor.constraint(equalTo: self.VM.tabbarVC.view.trailingAnchor).isActive = true
    self.VM.tabbarVC.tabBarView.bottomAnchor.constraint(equalTo: self.VM.tabbarVC.view.bottomAnchor).isActive = true
    self.VM.tabbarVC.tabBarView.setup()
  }
  public func tabBar(){
    self.VM.tabbarVC.tabBar.isHidden = true
  }
  public func tabBarViewButton(){
    self.VM.tabbarVC.tabBarView.actionButton(button: UIButton())
    self.VM.managers.logic.tapButton()
  }
  
  public func addedAllView(){
    self.VM.tabbarVC.view.addSubview(self.VM.tabbarVC.tabBarView)
  }
  public func ananimusAttributesView(){
    self.VM.ananimusView.cornerRadius(5, true)
    self.VM.ananimusView.borderColor(.white, 2)
    self.VM.ananimusView.shadowColor(color: .white, radius: 5)
    self.VM.attributes.name              = self.VM.ananimusView.debugDescription
    self.VM.attributes.displayMode       = .dark
    self.VM.attributes.shadow            = .active(with: .init(opacity: 3, radius: 5))
    self.VM.attributes.position          = .center
    self.VM.attributes.displayDuration   = .infinity
    let widthConstraint                    = EKAttributes.PositionConstraints.Edge.ratio(value: 0.9)
    let heightConstraint                   = EKAttributes.PositionConstraints.Edge.constant(value: 250)
    self.VM.attributes.positionConstraints.size = .init(width: widthConstraint, height: heightConstraint)
    self.VM.attributes.roundCorners      = .all(radius: 7)
    self.VM.attributes.screenInteraction = .dismiss
    self.VM.attributes.entryInteraction  = .absorbTouches
    self.VM.attributes.scroll            = .enabled(swipeable: false, pullbackAnimation: .jolt)
    let offset = EKAttributes.PositionConstraints.KeyboardRelation.Offset(bottom: 70, screenEdgeResistance: 50)
    let keyboardRelation = EKAttributes.PositionConstraints.KeyboardRelation.bind(offset: offset)
    self.VM.attributes.positionConstraints.keyboardRelation = keyboardRelation
  }
  public func ananimusView(){
    guard !self.VM.tabbarVC.view.isDescendant(of: self.VM.ananimusView) else { return }
    
    SwiftEntryKit.dismiss()
    self.VM.ananimusView.cornerRadius(5, false)
    self.VM.ananimusView.borderColor(.white, 2)
    self.VM.ananimusView.shadowColor(color: .black, radius: 10)
    self.VM.ananimusView.snp.makeConstraints { (ananimusView) in
      ananimusView.left.equalTo(self.VM.tabbarVC.view).inset(10)
      ananimusView.right.equalTo(self.VM.tabbarVC.view).inset(10)
      ananimusView.height.equalTo(250)
      ananimusView.centerY.equalTo(self.VM.tabbarVC.view.center)
    }
  }
}
//MARK: - Initial
extension SetupMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainTabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}


