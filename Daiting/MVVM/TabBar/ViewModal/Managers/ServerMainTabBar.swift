import UIKit
import Protocols

class ServerMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModal!
  
  public func getInvitation(completion: @escaping Clousure<[CODAnket]?>){
    guard let userData = GVuserData else { return }
    //Request
    self.VM.serverFB.request(requestType: .GETInvitation, data: userData) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerPair ->, function: getAnkets -> data: [CODAnket]? ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getInvitation(completion: completion)
          }
        //Susses
        case .object(let object):
          let ankets = object as? [CODAnket]
          completion(ankets)
          print("Succesful data: class: ServerPair ->, function: getAnkets ->, data: [CODAnket]?")
          
      }
    }
  }
  public func deleteInvantion(){
    guard let userData = GVuserData else { return }
    //Request
    self.VM.serverFB.request(requestType: .DELETEInvantion, data: userData) { (serverResult) in}
  }
  
}
//MARK: - Initial
extension ServerMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainTabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}


