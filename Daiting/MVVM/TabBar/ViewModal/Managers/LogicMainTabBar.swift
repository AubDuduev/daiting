import UIKit
import Protocols
import SnapKit
import SwiftEntryKit

class LogicMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModal!
  
  //MARK: - Create View Controllers For TabBar
  public func createVcForTabBar() -> [UIViewController]{
    let chats   = UIStoryboard.createVCID(sbID: .Chats  , vcID: .ChatsVC)
    let likes   = UIStoryboard.createVCID(sbID: .Likes  , vcID: .LikesVC)
    let kissMe  = UIStoryboard.createVCID(sbID: .KissMe , vcID: .KissMeVC)
    let pair    = UIStoryboard.createVCID(sbID: .Pair   , vcID: .PairVC)
    let profile = UIStoryboard.createVCID(sbID: .Profile, vcID: .ProfileVC)
    let controllers = [chats, likes, pair, profile, kissMe]
    return controllers
  }
  public func tapButton() {
    self.VM.tabbarVC.tabBarView.actions = { index in
      self.VM.tabbarVC.selectedIndex = index
      self.changesWhenSwitchingTabBar(index: index)
    }
   
  }
  
  public func changesWhenSwitchingTabBar(index: Int){
    self.VM.ananimusView.updateData(index: index)
    self.VM.managers.server.deleteInvantion()
    self.viewDidLoadChild(index: index)
  }
  public func viewDidLoadChild(index: Int){
    switch index {
      case 0:
        self.VM.tabbarVC.viewControllers?[index].viewDidLoad()
      case 1:
        self.VM.tabbarVC.viewControllers?[index].viewDidLoad()
      default:
        break
    }
    
  }
  public func aceptClousureAnanimusView(){
    self.VM.ananimusView.aceptClousure = { [weak self] in
      guard let self = self else { return }
      guard let postAnket = self.VM.ananimusView.postAnket else { return }
      let ananimusData = AnanimusData(anket: postAnket)
      self.VM.managers.router.push(.AnanimusVC(ananimusData))
    }
  }
  public func subscribe(){
    self.VM.managers.server.getInvitation() { [weak self]  (ankets) in
      guard let self = self else { return }
      guard let ankets = ankets, !ankets.isEmpty else { return }
      self.VM.ananimusView.postAnket = ankets.first
      SwiftEntryKit.display(entry: self.VM.ananimusView, using: self.VM.attributes)
    }
  }
}
//MARK: - Initial
extension LogicMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: MainTabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}
