
import UIKit

class MainTabBarViewController: UITabBarController {
  
  //MARK: - ViewModel
  public var viewModal: MainTabBarViewModal!
  
  //MARK: - Public variable
  public let tabBarView   = TabBarView().loadNib()
  
  //MARK: - Outlets
  
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  private func initViewModal(){
    self.viewModal = MainTabBarViewModal(tabBarViewController: self)
  }
}
