
import UIKit
import MessageUI
import WebKit

extension KissMeViewController: MFMailComposeViewControllerDelegate, WKUIDelegate {
  
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
	}
}
