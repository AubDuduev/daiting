import UIKit
import Protocols
import MessageUI

class LogicKissMe: VMManager {
  
  //MARK: - Public variable
  public var VM: KissMeViewModal!
  
  public func share(){
    let controller = UIActivityViewController(activityItems: ["Поделись с друзями"], applicationActivities: nil)
    self.VM.VC.present(controller, animated: true, completion: nil)
  }
  public func sendEmail(){
    if MFMailComposeViewController.canSendMail() {
      let composer = MFMailComposeViewController()
      composer.mailComposeDelegate = self.VM.VC
      composer.setToRecipients([URLString.Url.support.rawValue])
      composer.setSubject("Приложение NIKAH.LIFE")
      composer.setMessageBody("Хотел бы обратится к вам за помощью", isHTML: false)
      self.VM.VC.present(composer, animated: true)
    } else {
      AlertEK.dеfault(title: .information, message: .noMailCompose)
    }
  }
}
//MARK: - Initial
extension LogicKissMe {
  
  //MARK: - Inition
  convenience init(viewModal: KissMeViewModal) {
    self.init()
    self.VM = viewModal
  }
}
