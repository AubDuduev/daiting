import UIKit
import Protocols

class AnimationKissMe: VMManager {
  
  //MARK: - Public variable
  public var VM: KissMeViewModal!
  
  
}
//MARK: - Initial
extension AnimationKissMe {
  
  //MARK: - Inition
  convenience init(viewModal: KissMeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

