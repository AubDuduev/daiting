
import UIKit
import Protocols

class SetupKissMe: VMManager {
  
  //MARK: - Public variable
  public var VM: KissMeViewModal!
  
  
}
//MARK: - Initial
extension SetupKissMe {
  
  //MARK: - Inition
  convenience init(viewModal: KissMeViewModal) {
    self.init()
    self.VM = viewModal
  }
}


