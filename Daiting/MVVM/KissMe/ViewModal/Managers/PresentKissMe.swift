
import UIKit
import Protocols

class PresentKissMe: VMManager {
  
  //MARK: - Public variable
  public var VM: KissMeViewModal!
  
  
}
//MARK: - Initial
extension PresentKissMe {
  
  //MARK: - Inition
  convenience init(viewModal: KissMeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

