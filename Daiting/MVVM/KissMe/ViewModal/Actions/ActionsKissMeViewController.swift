
import UIKit
import StoreKit

extension KissMeViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  @IBAction func rateAppButton(button: UIButton){
    SKStoreReviewController.requestReview()
  }
  @IBAction func shareButton(button: UIButton){
    self.viewModal.managers.logic.share()
  }
  @IBAction func sendEmailButton(button: UIButton){
    self.viewModal.managers.logic.sendEmail()
  }
}
