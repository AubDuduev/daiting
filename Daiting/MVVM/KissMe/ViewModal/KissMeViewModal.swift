
import Foundation
import Protocols

class KissMeViewModal: VMManagers {
	
	public var kissMeModal: KissMeModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: KissMeManagers!
  public var VC      : KissMeViewController!
  
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
  
}
//MARK: - Initial
extension KissMeViewModal {
  
  convenience init(viewController: KissMeViewController) {
    self.init()
    self.VC       = viewController
    self.managers = KissMeManagers(viewModal: self)
  }
}
