import UIKit

class KissMeViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: KissMeViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
   
  }
  private func initViewModal(){
    self.viewModal = KissMeViewModal(viewController: self)
  }
}
