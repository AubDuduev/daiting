
import UIKit

enum KissMeModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)
	
}

