
import Foundation
import Protocols

class KissMeManagers: VMManagers {
  
  let setup    : SetupKissMe!
  let server   : ServerKissMe!
  let present  : PresentKissMe!
  let logic    : LogicKissMe!
  let animation: AnimationKissMe!
  let router   : RouterKissMe!
  
  init(viewModal: KissMeViewModal) {
    
    self.setup     = SetupKissMe(viewModal: viewModal)
    self.server    = ServerKissMe(viewModal: viewModal)
    self.present   = PresentKissMe(viewModal: viewModal)
    self.logic     = LogicKissMe(viewModal: viewModal)
    self.animation = AnimationKissMe(viewModal: viewModal)
    self.router    = RouterKissMe(viewModal: viewModal)
  }
}

