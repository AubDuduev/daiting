import UIKit

class RegistrationViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: RegistrationViewModal!
  
  //MARK: - Outlets
  @IBOutlet weak var registrationView   : UIView!
  @IBOutlet weak var loginButtonLabel   : UIButton!
  @IBOutlet weak var loginButtonView    : UIView!
  @IBOutlet weak var emailTextField     : UITextField!
  @IBOutlet weak var passwordTextField  : UITextField!
  @IBOutlet weak var confirmTextField   : UITextField!
  @IBOutlet weak var nameUserTextField  : UITextField!
  @IBOutlet weak var sigmentControlView : UIView!
  @IBOutlet weak var loginSegmentControl: UISegmentedControl!
  
  //MARK: - Constraints
  @IBOutlet weak var registrationViewHeightConstant: NSLayoutConstraint!
  @IBOutlet weak var registrationViewCentrConstant : NSLayoutConstraint!
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = RegistrationViewModal(viewController: self)
  }
}
