import UIKit
import Protocols

class AnimationRegistration: VMManager {
  
  //MARK: - Public variable
  public var VM: RegistrationViewModal!
  
  public func bottomConstantSigmentView(change: Bool){
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
      if change {
        self.VM.VC.registrationViewHeightConstant.constant = 299
        self.VM.VC.confirmTextField.isHidden  = false
        self.VM.VC.nameUserTextField.isHidden = false
        self.VM.VC.loginButtonLabel.setTitle("Регистрация", for: .normal)
      } else {
        self.VM.VC.registrationViewHeightConstant.constant = 199
        self.VM.VC.confirmTextField.isHidden  = true
        self.VM.VC.nameUserTextField.isHidden = true
        self.VM.VC.loginButtonLabel.setTitle("Вход", for: .normal)
      }
      self.VM.VC.view.layoutIfNeeded()
    }) {(finish) in
     
    }
  }
}
//MARK: - Initial
extension AnimationRegistration {
  
  //MARK: - Inition
  convenience init(viewModal: RegistrationViewModal) {
    self.init()
    self.VM = viewModal
  }
}

