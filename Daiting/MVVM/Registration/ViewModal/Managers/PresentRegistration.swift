
import UIKit
import Protocols

class PresentRegistration: VMManager {
  
  //MARK: - Public variable
  public var VM: RegistrationViewModal!
  
  
}
//MARK: - Initial
extension PresentRegistration {
  
  //MARK: - Inition
  convenience init(viewModal: RegistrationViewModal) {
    self.init()
    self.VM = viewModal
  }
}

