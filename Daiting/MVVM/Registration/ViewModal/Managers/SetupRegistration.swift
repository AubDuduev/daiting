
import UIKit
import Protocols
import AnimatedGradientView

class SetupRegistration: VMManager {
  
  //MARK: - Public variable
  public var VM: RegistrationViewModal!
  
  //MARK: - RegistrationViewController
  public func segmentControll(){
    let atributes = [NSAttributedString.Key.foregroundColor : UIColor.white,
                     NSAttributedString.Key.font : UIFont.set(.lemon, 15)]
    self.VM.VC.loginSegmentControl.setTitleTextAttributes(atributes, for: .normal)
  }
  public func registrationView(){
    self.VM.VC.registrationView.cornerRadius(9, false)
    self.VM.VC.registrationView.shadowColor(color: .black, radius: 5)
  }
  public func loginButtonView(){
    self.VM.VC.loginButtonView.cornerRadius(20, true)
    self.VM.VC.loginButtonView.shadowColor(color: .black, radius: 2)
    self.VM.VC.loginButtonLabel.setTitleColor(.white, for: .normal)
    self.VM.VC.loginButtonLabel.titleLabel?.font = UIFont.set(.lemon, 13)
  }
  public func segmentView(){
    self.VM.VC.sigmentControlView.cornerRadius(8, false)
    self.VM.VC.sigmentControlView.shadowColor(color: .black, radius: 2)
  }
  public func addNotification(){
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.logic.animationRegistrationView(notification: notification, show: true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.logic.animationRegistrationView(notification: notification, show: false)
    }
  }
  public func viewAddViewGradient(){
    let animatedGradient = AnimatedGradientView(frame: self.VM.VC.view.frame)
    animatedGradient.direction = .upLeft
    animatedGradient.colors = [[#colorLiteral(red: 0.9098039269, green: 0.3012256145, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)]]
    if !self.VM.VC.view.isDescendant(of: animatedGradient) {
      self.VM.VC.view.insertSubview(animatedGradient, at: 0)
    }
  }
}
//MARK: - Initial
extension SetupRegistration {
  
  //MARK: - Inition
  convenience init(viewModal: RegistrationViewModal) {
    self.init()
    self.VM = viewModal
  }
}


