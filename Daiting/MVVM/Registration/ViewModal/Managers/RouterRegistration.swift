
import UIKit
import Protocols

class RouterRegistration: VMManager {
  
  //MARK: - Public variable
  public var VM: RegistrationViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .MainNavBarVC:
        self.pushMainNavBarVC()
    }
  }
  
  enum Push {
    case MainNavBarVC
  }
}
//MARK: - Private functions
extension RouterRegistration {
  
  private func pushMainNavBarVC(){
    let mainNavBarVC = self.VM.VC.getVCForID(storyboardID     : .MainNavBar,
                                             vcID             : .MainNavBarVC,
                                             transitionStyle  : .crossDissolve,
                                             presentationStyle: .fullScreen) as! MainNavBarViewController
    self.VM.VC.present(mainNavBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterRegistration {
  
  //MARK: - Inition
  convenience init(viewModal: RegistrationViewModal) {
    self.init()
    self.VM = viewModal
  }
}



