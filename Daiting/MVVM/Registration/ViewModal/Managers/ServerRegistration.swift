import UIKit
import Protocols

class ServerRegistration: VMManager {
  
  //MARK: - Public variable
  public var VM: RegistrationViewModal!
  
  public func saveUser(userData: CODUserData, completion: @escaping Clousure<CODUserData>){
    //Request
    self.VM.serverFB.request(requestType: .POSTUserData, data: userData) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerRegistration ->, function: saveUser -> data: CODUserData ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.saveUser(userData: userData, completion: completion)
          }
        //Susses
        case .object(let object):
          let userData  = object as! CODUserData
          completion(userData)
          print("Succesful data: class: ServerRegistration ->, function: saveUser ->, data: CODUserData")
        
      }
    }
  }
}
//MARK: - Initial
extension ServerRegistration {
  
  //MARK: - Inition
  convenience init(viewModal: RegistrationViewModal) {
    self.init()
    self.VM = viewModal
  }
}


