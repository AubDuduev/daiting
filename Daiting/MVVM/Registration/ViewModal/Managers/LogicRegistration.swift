import Foundation
import Protocols
import UIKit
import FirebaseAuth

class LogicRegistration: VMManager {
  
  //MARK: - Public variable
  public var VM: RegistrationViewModal!
  
  public func segmentSwitch(control: UISegmentedControl){
    if control.selectedSegmentIndex == 0 {
      self.VM.managers.animation.bottomConstantSigmentView(change: true)
    } else {
      self.VM.managers.animation.bottomConstantSigmentView(change: false)
    }
  }
  public func auth(control: UISegmentedControl){
    switch control.selectedSegmentIndex {
      case 0:
        self.registration()
      case 1:
        self.entrance()
      default:
        break
    }
  }
  @objc
  public func animationRegistrationView(notification: Notification?, show: Bool){
    guard let userInfo = notification?.userInfo else { return }
    let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
    UIView.animate(withDuration: 0.5) {
      if show {
        let raznica = self.VM.VC.registrationView.frame.maxY - keyboardFrame.minY
        guard raznica > 0 else { return }
        self.VM.VC.registrationViewCentrConstant.constant = -(raznica + 50)
      } else {
        self.VM.VC.registrationViewCentrConstant.constant = 0
      }
      self.VM.VC.view.layoutIfNeeded()
    }
  }
}
//MARK: - Private functions
extension LogicRegistration {
  
  //MARK: - Entrance user logic
  private func entrance(){
    //1 - проверяем поля текст фиелд для входа
    guard self.VM.VC.emailTextField.check(), self.VM.VC.passwordTextField.check() else {
      AlertEK.dеfault(title: .error, message: .emptyField)
      return
    }
    //2 - Входим
    let email    = self.VM.VC.emailTextField.text ?? ""
    let password = self.VM.VC.passwordTextField.text ?? ""
    self.signIn(email: email, password: password){ succes in
      //3 - Получаем данные пользователя
      guard succes else { return }
      self.firstResponder()
      //6 - Переходим на таб бар
      AlertEK.dеfault(title: .information, message: .succesRegistration) {
        self.VM.managers.router.push(.MainNavBarVC)
      }
    }
  }
  private func signIn(email: String, password: String, completion: @escaping Clousure<Bool>){
    Auth.auth().signIn(withEmail: email, password: password) { (auth, error) in
      
      if let error = error {
        AlertEK.customText(title: .error, message: .message(error.localizedDescription))
        completion(false)
      } else {
        completion(true)
      }
    }
  }
  //MARK: - Registration Logic
  private func registration(){
    
    //1 - проверяем поля текст фиелд для регистрации
    guard self.VM.VC.emailTextField.check(), self.VM.VC.passwordTextField.check(),
      self.VM.VC.confirmTextField.check(), self.VM.VC.nameUserTextField.check() else {
        AlertEK.dеfault(title: .error, message: .emptyField)
      return
    }
    //2 - проверка пароля
    guard self.VM.VC.passwordTextField.compare(field: self.VM.VC.confirmTextField) else {
      AlertEK.dеfault(title: .error, message: .passworNotConfirm)
      return
    }
    //3 - Регистрируемся
    var email    = self.VM.VC.emailTextField.text ?? ""
    let password = self.VM.VC.passwordTextField.text ?? ""
    email = email.lowercased()
    //4 - Cоздаем нового пользователя
    self.createUser(email: email, password: password, completion: { succes in
      guard succes else { return }
      //5 - Сохроняем данные пользователя в базе данных
      self.saveDateUser(){ [weak self] in
        guard let self = self else { return }
        self.firstResponder()
        //6 - Переходим на таб баp
        AlertEK.dеfault(title: .information, message: .succesRegistration) {
          self.VM.managers.router.push(.MainNavBarVC)
        }
      }
    })
  }
  private func createUser(email: String, password: String, completion: @escaping Clousure<Bool>){
    Auth.auth().createUser(withEmail: email, password: password) { (auth, error) in
      
      if let error = error {
        AlertEK.customText(title: .error, message: .message(error.localizedDescription))
        completion(false)
      } else {
        completion(true)
      }
    }
  }
  private func saveDateUser(completion: @escaping ClousureEmpty){
    let email = self.VM.VC.emailTextField.text ?? ""
    let name  = self.VM.VC.nameUserTextField.text ?? ""
    let id    = Auth.auth().currentUser?.uid
    let user  = CODUserData(ID: id, email: email, name: name)
    self.VM.managers.server.saveUser(userData: user) { (userData) in
      GVuserData = userData
      completion()
    }
  }
  private func firstResponder(){
    self.VM.VC.emailTextField.resignFirstResponder()
    self.VM.VC.passwordTextField.resignFirstResponder()
    self.VM.VC.confirmTextField.resignFirstResponder()
    self.VM.VC.nameUserTextField.resignFirstResponder()
  }
}

//MARK: - Initial
extension LogicRegistration {
  
  //MARK: - Inition
  convenience init(viewModal: RegistrationViewModal) {
    self.init()
    self.VM = viewModal
  }
}
