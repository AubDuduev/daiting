
import Foundation
import Protocols
import SwiftEntryKit

class RegistrationViewModal: VMManagers {
	
	public var registrationModal: RegistrationModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers           : RegistrationManagers!
  public var VC                 : RegistrationViewController!
  public var errorHandlerAnkets = ErrorHandlerAnkets()
  public let serverFB           = ServerFB()
  public var attributesRV       = EKAttributes()
  
  public func viewDidLoad() {
    self.managers.setup.addNotification()
    self.managers.setup.segmentControll()
    self.commonLogic()
  }
  
  public func viewDidLayoutSubviews() {
    self.managers.setup.registrationView()
    self.managers.setup.loginButtonView()
    self.managers.setup.segmentView()
    self.managers.setup.segmentControll()
    self.managers.setup.viewAddViewGradient()
  }
  
  private func commonLogic(){
    
    switch self.registrationModal {
      //1 - Загрузка
      case .loading:
        self.registrationModal = .getData
      //2 - Получаем данные
      case .getData:
       print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension RegistrationViewModal {
  
  convenience init(viewController: RegistrationViewController) {
    self.init()
    self.VC       = viewController
    self.managers = RegistrationManagers(viewModal: self)
  }
}
