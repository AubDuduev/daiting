
import UIKit

extension RegistrationViewController {
  
  @IBAction func loginButton(button: UIButton){
    self.viewModal.managers.logic.auth(control: self.loginSegmentControl)
  }
  @IBAction func pushMenuButton(button: UIButton){
    self.viewModal.managers.router.push(.MainNavBarVC)
  }
  @IBAction func registrationSegment(control: UISegmentedControl){
    self.viewModal.managers.logic.segmentSwitch(control: control)
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.emailTextField.resignFirstResponder()
    self.passwordTextField.resignFirstResponder()
    self.confirmTextField.resignFirstResponder()
    self.nameUserTextField.resignFirstResponder()
  }
}
