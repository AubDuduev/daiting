
import Foundation
import Protocols

class RegistrationManagers: VMManagers {
  
  let setup    : SetupRegistration!
  let server   : ServerRegistration!
  let present  : PresentRegistration!
  let logic    : LogicRegistration!
  let animation: AnimationRegistration!
  let router   : RouterRegistration!
  
  init(viewModal: RegistrationViewModal) {
    
    self.setup     = SetupRegistration(viewModal: viewModal)
    self.server    = ServerRegistration(viewModal: viewModal)
    self.present   = PresentRegistration(viewModal: viewModal)
    self.logic     = LogicRegistration(viewModal: viewModal)
    self.animation = AnimationRegistration(viewModal: viewModal)
    self.router    = RouterRegistration(viewModal: viewModal)
  }
}

