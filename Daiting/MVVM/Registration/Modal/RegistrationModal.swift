
import UIKit

enum RegistrationModal {
	
	case loading
	case getData
	case errorHandler(RegistrationData?)
	case presentData(RegistrationData)

}

