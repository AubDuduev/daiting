
import Foundation
import Protocols

class LoadingManagers: VMManagers {
  
  let setup    : SetupLoading!
  let server   : ServerLoading!
  let present  : PresentLoading!
  let logic    : LogicLoading!
  let animation: AnimationLoading!
  let router   : RouterLoading!
  
  init(viewModal: LoadingViewModal) {
    
    self.setup     = SetupLoading(viewModal: viewModal)
    self.server    = ServerLoading(viewModal: viewModal)
    self.present   = PresentLoading(viewModal: viewModal)
    self.logic     = LogicLoading(viewModal: viewModal)
    self.animation = AnimationLoading(viewModal: viewModal)
    self.router    = RouterLoading(viewModal: viewModal)
  }
}

