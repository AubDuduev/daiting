import UIKit

class LoadingViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: LoadingViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  private func initViewModal(){
    self.viewModal = LoadingViewModal(viewController: self)
  }
}
