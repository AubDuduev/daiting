
import Protocols
import Foundation

class LoadingViewModal: VMManagers {
	
	public var loadingModal: LoadingModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers : LoadingManagers!
  public var VC       : LoadingViewController!
  public var serverFB = ServerFB()
  
  public func viewDidLoad() {
    self.commonLogic()
  }
 
  public func viewDidAppear() {
    self.managers.router.push(.MainNavBarVC)
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    self.managers.logic.getUserData()
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension LoadingViewModal {
  
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = LoadingManagers(viewModal: self)
  }
}
