
import UIKit
import Protocols

class SetupLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  
}
//MARK: - Initial
extension SetupLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}


