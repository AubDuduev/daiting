import Foundation
import Protocols
import FirebaseAuth

class LogicLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  public func getUserData(){
    guard let email = self.checkRegistration() else { return }
    // 1 - Получем вежие данные пользователя
    self.VM.managers.server.getUserData(email: email) { (userData) in
      //2 - СОхроняем их
      GVuserData = userData
    }
  }
}
//MARK: - Private functions
extension LogicLoading {
  
  private func checkRegistration() -> String? {
    
    if let email = Auth.auth().currentUser?.email {
      return email
    } else {
      GVuserData = nil
      return nil
    }
  }
}
//MARK: - Initial
extension LogicLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}
