
import UIKit
import Protocols

class RouterLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .MainNavBarVC:
        self.pushMainNavBarVC()
    }
  }
  
  enum Push {
    case MainNavBarVC
  }
}
//MARK: - Private functions
extension RouterLoading {
  
  private func pushMainNavBarVC(){
    let mainNavBarVC = self.VM.VC.getVCForID(storyboardID     : .MainNavBar,
                                             vcID             : .MainNavBarVC,
                                             transitionStyle  : .crossDissolve,
                                             presentationStyle: .fullScreen) as! MainNavBarViewController
    self.VM.VC.present(mainNavBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}



