import UIKit
import Protocols

class ServerLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  public func getUserData(email: String, completion: @escaping Clousure<CODUserData>){
    //Request
    self.VM.serverFB.request(requestType: .GETUserData, data: email) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerLoading ->, function: getUserData -> data: CODUserData ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getUserData(email: email, completion: completion)
          }
        //Susses
        case .object(let object):
          let userData  = object as! CODUserData
          completion(userData)
          print("Succesful data: class: ServerLoading ->, function: getUserData ->, data: CODUserData")
        
      }
    }
  }
}
//MARK: - Initial
extension ServerLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}


