
import UIKit

enum ProfileModal {
	
	case loading
	case getData
	case errorHandler(CODAnket?)
	case presentData(ProfileData)
	
}

