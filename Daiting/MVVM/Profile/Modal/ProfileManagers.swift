
import Foundation
import Protocols

class ProfileManagers: VMManagers {
  
  let setup    : SetupProfile!
  let server   : ServerProfile!
  let present  : PresentProfile!
  let logic    : LogicProfile!
  let animation: AnimationProfile!
  let router   : RouterProfile!
  
  init(viewModal: ProfileViewModal) {
    
    self.setup     = SetupProfile(viewModal: viewModal)
    self.server    = ServerProfile(viewModal: viewModal)
    self.present   = PresentProfile(viewModal: viewModal)
    self.logic     = LogicProfile(viewModal: viewModal)
    self.animation = AnimationProfile(viewModal: viewModal)
    self.router    = RouterProfile(viewModal: viewModal)
  }
}

