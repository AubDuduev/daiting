
import UIKit

struct ProfileData {
  
}

enum Gender: String {
  
  case male   = "Мужской"
  case female = "Женский"
  
  static func getIndex(string: String?) -> Int {
    let gender = Gender(rawValue: string ?? "")
    
    switch gender {
      case .male:
        return 0
      case .female:
        return 1
      default:
        return 1
    }
  }
  static func getName(segment: UISegmentedControl) -> Gender {
  
    switch segment.selectedSegmentIndex {
      case 0:
        return .male
      case 1:
        return .female
      default:
        return .male
    }
  }
}

enum Status: String {
  
  case free   = "Свободен"
  case noFree = "В паре"
  
  static func getIndex(string: String?) -> Int {
    let status = Status(rawValue: string ?? "")
    
    switch status {
      case .free:
        return 0
      case .noFree:
        return 1
      default:
        return 1
    }
  }
  static func getName(segment: UISegmentedControl) -> Status {
  
    switch segment.selectedSegmentIndex {
      case 0:
        return .free
      case 1:
        return .noFree
      default:
        return .free
    }
  }
}

enum SearchGender: String {
  
  case man   = "Мужчину"
  case woman = "Девушку"
  
  static func getIndex(string: String?) -> Int {
    let searchGender = SearchGender(rawValue: string ?? "")
    
    switch searchGender {
      case .man:
        return 0
      case .woman:
        return 1
      default:
        return 1
    }
  }
  static func getName(segment: UISegmentedControl) -> SearchGender {
  
    switch segment.selectedSegmentIndex {
      case 0:
        return .man
      case 1:
        return .woman
      default:
        return .man
    }
  }
}
