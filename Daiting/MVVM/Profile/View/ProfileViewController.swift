import UIKit

class ProfileViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: ProfileViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet weak var commonView         : UIView!
  @IBOutlet weak var commonScrollView   : UIScrollView!
  @IBOutlet weak var registrationView   : UIView!
  @IBOutlet weak var chantView          : UIView!
  @IBOutlet weak var avatarFonView      : UIView!
  @IBOutlet weak var avatarImageView    : UIImageView!
  @IBOutlet weak var entranceButtonLabel: UIButton!
  @IBOutlet weak var nameTextField      : UITextField!
  @IBOutlet weak var cityTextField      : UITextField!
  @IBOutlet weak var ageTextField       : UITextField!
  @IBOutlet weak var changeStatusButton : UIButton!
  @IBOutlet weak var changeGenderButton : UIButton!
  @IBOutlet weak var changeSearchButton : UIButton!
  
  @IBOutlet var segmentControlls: [UISegmentedControl]!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.viewModal.viewWillDisappear()
  }
  private func initViewModal(){
    self.viewModal = ProfileViewModal(viewController: self)
  }
}
