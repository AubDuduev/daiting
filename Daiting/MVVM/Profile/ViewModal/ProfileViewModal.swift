
import Foundation
import Protocols

class ProfileViewModal: VMManagers {
	
	public var profileModal: ProfileModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers    : ProfileManagers!
  public var VC          : ProfileViewController!
  public let serverFB    = ServerFB()
  public var photoLibrary: PhotoLibrary!
  
  public func viewDidLoad() {
    self.managers.setup.addNotification()
    self.managers.setup.photoLibrary()
    self.managers.setup.textFields()
    self.managers.setup.segmentControlls()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.registrationView()
    self.managers.setup.chantView()
    self.managers.setup.avatarFonView()
    self.managers.setup.avatarImageView()
  }
  public func viewDidAppear() {
    self.managers.logic.getUserData()
    self.managers.animation.signOutView()
    
  }
  public func viewWillDisappear() {
    self.managers.logic.changeUserData()
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension ProfileViewModal {
  
  convenience init(viewController: ProfileViewController) {
    self.init()
    self.VC       = viewController
    self.managers = ProfileManagers(viewModal: self)
  }
}
