
import UIKit

extension ProfileViewController {
  
  @IBAction func signOutButton(button: UIButton){
    self.viewModal.managers.logic.signOutRegistration()
  }
  @IBAction func changePhotoButton(button: UIButton){
    self.viewModal.managers.logic.changePhoto()
  }
  @IBAction func changeStatusButton(segment: UISegmentedControl){
    self.viewModal.managers.logic.changeStatus(segment: segment)
  }
  @IBAction func changeGenderButton(segment: UISegmentedControl){
    self.viewModal.managers.logic.changeGender(segment: segment)
  }
  @IBAction func changeSearchButton(segment: UISegmentedControl){
    self.viewModal.managers.logic.changeSearch(segment: segment)
  }
  @IBAction func commonScrollViewGesure(gesture: UITapGestureRecognizer){
    self.viewModal.managers.logic.resignFirstResponder()
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.viewModal.managers.logic.resignFirstResponder()
  }
}
