
import UIKit 

extension ProfileViewController: UITextFieldDelegate {

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.viewModal.managers.logic.resignFirstResponder()
    return true
  }
}
