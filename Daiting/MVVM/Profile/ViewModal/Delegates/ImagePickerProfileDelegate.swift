//
//  ImagePickerProfileDelegate.swift
import UIKit

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    if let image = info[.editedImage] as? UIImage {
      self.avatarImageView.image = image
    } else if let image = info[.originalImage] as? UIImage {
      self.avatarImageView.image = image
    }
    self.viewModal.managers.logic.uploadUserPhoto()
    picker.dismiss(animated: true)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true)
  }
}

