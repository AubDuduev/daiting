import UIKit
import Protocols
import FirebaseAuth

class AnimationProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModal!
  
  public func signOutView(){
    UIView.animate(withDuration: 1) {
      if let _ = Auth.auth().currentUser {
        self.VM.VC.entranceButtonLabel.setTitle("Выход", for: .normal)
      } else {
        self.VM.VC.entranceButtonLabel.setTitle("Регистрация", for: .normal)
      }
    }
  }
}
//MARK: - Initial
extension AnimationProfile {
  
  //MARK: - Inition
  convenience init(viewModal: ProfileViewModal) {
    self.init()
    self.VM = viewModal
  }
}

