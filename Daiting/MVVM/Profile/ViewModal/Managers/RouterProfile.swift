
import UIKit
import Protocols

class RouterProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .RegistrationVC:
        self.pushRegistrationVC()
    }
  }
  
  enum Push {
    case RegistrationVC
  }
}
//MARK: - Private functions
extension RouterProfile {
  
  private func pushRegistrationVC(){
    let registrVC = self.VM.VC.getVC(storyboardID     : .Registration,
                                     animation        : true,
                                     transitionStyle  : .crossDissolve,
                                     presentationStyle: .fullScreen) as! RegistrationViewController
    UIApplication.shared.windows.first?.rootViewController = registrVC
    UIApplication.shared.windows.first?.makeKey()
    self.VM.VC.present(registrVC, animated: true)
  }
  
}
//MARK: - Initial
extension RouterProfile {
  
  //MARK: - Inition
  convenience init(viewModal: ProfileViewModal) {
    self.init()
    self.VM = viewModal
  }
}



