
import UIKit
import Protocols
import AnimatedGradientView
import SnapKit

class SetupProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModal!
  
  public func registrationView(){
    let animatedGradient = AnimatedGradientView()
    animatedGradient.direction = .upLeft
    animatedGradient.colors = [[.set(.oneGradient), .set(.twoGradient)]]
    animatedGradient.cornerRadius(25, true)
    self.VM.VC.registrationView.cornerRadius(25, false)
    self.VM.VC.registrationView.shadowColor(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5008603051), radius: 5)
    self.VM.VC.registrationView.insertSubview(animatedGradient, at: 0)
    animatedGradient.snp.makeConstraints { (animatedGradient) in
      animatedGradient.edges.equalTo(self.VM.VC.registrationView)
    }
  }
  public func chantView(){
    let corner = self.VM.VC.chantView.frame.width / 2
    self.VM.VC.chantView.cornerRadius(corner, false)
    self.VM.VC.chantView.shadowColor(color: #colorLiteral(red: 0.1176470588, green: 0, blue: 0, alpha: 0.5), radius: 5)
    self.VM.VC.chantView.borderColor(.set(.twoGradient), 2)
  }
  public func addNotification(){
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.logic.changeContentScrollView(notification: notification, show: true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)
    {(notification) in
      self.VM.managers.logic.changeContentScrollView(notification: notification, show: false)
    }
  }
  public func avatarFonView(){
    self.VM.VC.avatarFonView.cornerRadius(50, true)
    self.VM.VC.avatarFonView.shadowColor(color: #colorLiteral(red: 0.8509803922, green: 0.5843137255, blue: 0.09411764706, alpha: 0.2380611796), radius: 0)
    self.VM.VC.avatarFonView.borderColor( #colorLiteral(red: 0.8509803922, green: 0.5843137255, blue: 0.09411764706, alpha: 0.2380611796), 0)
  }
  public func textFields(){
    self.VM.VC.nameTextField?.returnKeyType = .done
    self.VM.VC.cityTextField?.returnKeyType = .done
    self.VM.VC.ageTextField?.returnKeyType  = .done
  }
  public func photoLibrary(){
    self.VM.photoLibrary = PhotoLibrary(viewController: self.VM.VC)
    self.VM.photoLibrary.setup()
  }
  public func segmentControlls(){
    let atributes = [NSAttributedString.Key.foregroundColor : UIColor.set(.twoGradient),
                     NSAttributedString.Key.font : UIFont.set(.lemon, 10)]
    self.VM.VC.segmentControlls.forEach({$0.setTitleTextAttributes(atributes, for: .normal)})
  }
  public func avatarImageView(){
    self.VM.VC.avatarImageView.cornerRadius(70, true)
    self.VM.VC.avatarImageView.borderColor(#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), 1)
  }
}
//MARK: - Initial
extension SetupProfile {
  
  //MARK: - Inition
  convenience init(viewModal: ProfileViewModal) {
    self.init()
    self.VM = viewModal
  }
}


