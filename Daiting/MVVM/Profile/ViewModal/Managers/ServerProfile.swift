import UIKit
import Protocols

class ServerProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModal!
  
  public func getUserData(email: String, completion: @escaping Clousure<CODUserData>){
    //Request
    self.VM.serverFB.request(requestType: .GETUserData, data: email) { [unowned self] (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerRegistration ->, function: getUserData -> data: CODUserData ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.getUserData(email: email, completion: completion)
          }
        //Susses
        case .object(let object):
          let userData  = object as! CODUserData
          completion(userData)
          print("Succesful data: class: ServerRegistration ->, function: getUserData ->, data: CODUserData")
        
      }
    }
  }
  public func setUsers(data: CODUserData){
    //Request
    self.VM.serverFB.request(requestType: .POSTUsers, data: data) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerProfile ->, function: setUsers -> data: CODUserData ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.setUsers(data: data)
          }
        //Susses
        case .object(let object):
          let _ = object as! CODUserData
          print("Succesful data: class: ServerProfile ->, function: setUsers ->, data: CODUserData")
          
      }
    }
  }
  public func updateUserData(){
    //Request
    self.VM.serverFB.request(requestType: .UPDATEUserData, data: GVuserData) {(serverResult) in
    }
  }
  public func uploadImage(data: Data, completion: @escaping Clousure<String?>){
    //Request
    self.VM.serverFB.request(requestType: .POSTImage, data: data) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerProfile ->, function: uploadImage -> data: String ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){
            self.uploadImage(data: data, completion: completion)
          }
        //Susses
        case .object(let object):
          let urlString = object as! String
          completion(urlString)
          print("Succesful data: class: ServerProfile ->, function: uploadImage ->, data: String")
          
      }
    }
  }
}
//MARK: - Initial
extension ServerProfile {
  
  //MARK: - Inition
  convenience init(viewModal: ProfileViewModal) {
    self.init()
    self.VM = viewModal
  }
}


