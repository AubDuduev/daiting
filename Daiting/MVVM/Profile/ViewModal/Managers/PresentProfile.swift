
import UIKit
import Protocols

class PresentProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModal!
  
  public func setUserData(){
    self.VM.VC.nameTextField?.text   = GVuserData?.name
    self.VM.VC.cityTextField?.text   = GVuserData?.city
    self.VM.VC.ageTextField?.text    = GVuserData?.age
    self.VM.VC.segmentControlls[0].selectedSegmentIndex = Gender.getIndex(string: GVuserData?.gender)
    self.VM.VC.segmentControlls[1].selectedSegmentIndex = Status.getIndex(string: GVuserData?.status)
    self.VM.VC.segmentControlls[2].selectedSegmentIndex = SearchGender.getIndex(string: GVuserData?.searchGender)
    self.VM.managers.animation.signOutView()
    self.VM.managers.logic.changeStatus(segment: self.VM.VC.segmentControlls[1])
    self.VM.managers.logic.changeGender(segment: self.VM.VC.segmentControlls[0])
    self.VM.managers.logic.changeSearch(segment: self.VM.VC.segmentControlls[2])
  }
  public func image(){
    guard let image = GVuserData?.avatar else { return }
    guard let url   = URL(string: image) else { return }
    self.VM.VC.avatarImageView.isSkeletonable = true
    self.VM.VC.avatarImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .set(.navBarFon)))
    self.VM.VC.avatarImageView.sd_setImage(with: url) { [weak self] (_, error, _, url) in
      guard let self = self else { return }
      if let error = error {
        print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
      } else {
        self.VM.VC.avatarImageView.hideSkeleton()
      }
    }
  }
}
//MARK: - Initial
extension PresentProfile {
  
  //MARK: - Inition
  convenience init(viewModal: ProfileViewModal) {
    self.init()
    self.VM = viewModal
  }
}

