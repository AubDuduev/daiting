import Foundation
import Protocols
import FirebaseAuth
import SwiftEntryKit

class LogicProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModal!
  
  public func signOutRegistration(){
    if self.VM.VC.entranceButtonLabel.currentTitle == "Выход" {
      AlertEK.options(title: .information, message: .exitProfile, options: .Cancel) { [weak self] (responce) in
        guard let self = self else { return }
        if responce == 0 {
          do {
            try Auth.auth().signOut()
          } catch let error {
            print(error.localizedDescription)
          }
          self.VM.managers.animation.signOutView()
          self.getUserData()
        }
      }
    } else {
      self.VM.managers.router.push(.RegistrationVC)
    }
  }
  public func changeUserData(){
    self.VM.VC.cityTextField.resignFirstResponder()
    self.VM.VC.ageTextField.resignFirstResponder()
    guard GVuserData != nil else {
     
      return
    }
    self.saveUserData()
  }
  public func changePhoto(){
    AlertEK.options(title: .addPhoto, message: .addPhotoChange, options: .AddPhoto) { (index) in
      switch index {
        case 0:
          self.VM.photoLibrary.library()
        case 1:
          self.VM.photoLibrary.camera()
        default:
          SwiftEntryKit.dismiss()
      }
    }
  }
  public func getUserData(){
    guard let email = self.checkRegistration() else { self.VM.managers.present.setUserData(); return }
    // 1 - Получем вежие данные пользователя
    self.VM.managers.server.getUserData(email: email) { (userData) in
      //2 - СОхроняем их
      GVuserData = userData
      //3 - Презентуем
      self.VM.managers.present.setUserData()
      self.VM.managers.present.image()
    }
  }
  @objc
  public func changeContentScrollView(notification: Notification?, show: Bool){
    guard let userInfo = notification?.userInfo else { return }
    let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
    UIView.animate(withDuration: 0.5) {
      if show {
        let resultSuze = self.VM.VC.commonView.bounds.height + keyboardFrame.height
        self.VM.VC.commonScrollView.contentSize.height = resultSuze
      } else {
        self.VM.VC.commonScrollView.contentSize.height = self.VM.VC.commonView.bounds.height
      }
      self.VM.VC.view.layoutIfNeeded()
    }
  }
  public func resignFirstResponder(){
    self.VM.VC.nameTextField?.resignFirstResponder()
    self.VM.VC.cityTextField?.resignFirstResponder()
    self.VM.VC.ageTextField?.resignFirstResponder()
  }
  public func uploadUserPhoto(){
    let image = self.VM.VC.avatarImageView.image?.jpegData(compressionQuality: 1)
    guard let data = image else { return }
    self.VM.managers.server.uploadImage(data: data) { (urlString) in
      
      GVuserData.avatar = urlString
      self.changeUserData()
    }
  }
  public func changeStatus(segment: UISegmentedControl){
    let image = (segment.selectedSegmentIndex == 0) ? UIImage.set(.freeWedding) : UIImage.set(.wedding)
    self.VM.VC.changeStatusButton.setImage(image, for: .normal)
  }
  public func changeGender(segment: UISegmentedControl){
    let image = (segment.selectedSegmentIndex == 0) ? UIImage.set(.male) : UIImage.set(.female)
    self.VM.VC.changeGenderButton.setImage(image, for: .normal)
  }
  public func changeSearch(segment: UISegmentedControl){
    let image = (segment.selectedSegmentIndex == 0) ? UIImage.set(.female) : UIImage.set(.male)
    self.VM.VC.changeSearchButton.setImage(image, for: .normal)
  }
}
//MARK: - Private functions
extension LogicProfile {
  
  private func saveUserData(){
    guard GVuserData != nil else { return }
    GVuserData.name         = self.VM.VC.nameTextField?.text
    GVuserData.city         = self.VM.VC.cityTextField?.text
    GVuserData.age          = self.VM.VC.ageTextField?.text
    GVuserData.gender       = Gender.getName(segment: self.VM.VC.segmentControlls[0]).rawValue
    GVuserData.status       = Status.getName(segment: self.VM.VC.segmentControlls[1]).rawValue
    GVuserData.searchGender = SearchGender.getName(segment: self.VM.VC.segmentControlls[2]).rawValue
    self.VM.managers.server.updateUserData()
    self.VM.managers.server.setUsers(data: GVuserData)
  }
  private func checkRegistration() -> String? {
    
    if let email = Auth.auth().currentUser?.email {
      self.VM.managers.animation.signOutView()
      return email
    } else {
      GVuserData = nil
      self.VM.managers.animation.signOutView()
      return nil
    }
  }
}

//MARK: - Initial
extension LogicProfile {
  
  //MARK: - Inition
  convenience init(viewModal: ProfileViewModal) {
    self.init()
    self.VM = viewModal
  }
}

