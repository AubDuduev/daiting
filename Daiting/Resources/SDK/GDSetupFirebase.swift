
import Foundation
import UserNotifications
import FirebaseMessaging
import FirebaseAuth
import FirebaseAnalytics
import FirebaseFirestore
import FirebaseRemoteConfig
import FirebaseCore

class GDSetupFirebase: NSObject {
  
  public func setup(){
    FirebaseApp.configure()
  }
  
  private func remoteConfig() -> RemoteConfig {
    let remoteConfig = RemoteConfig.remoteConfig()
    let settings     = RemoteConfigSettings()
    settings.minimumFetchInterval = 0
    remoteConfig.configSettings = settings
    return remoteConfig
  }
  public func getAdvertising(completion: @escaping Clousure<Bool>){
    self.remoteConfig().fetchAndActivate { (status, error) in
      DispatchQueue.main.async {
        if status == .error {
          completion(false)
        } else {
          print(self.remoteConfig()["advertising"].boolValue, " - remote configuration advertising")
          
          completion(self.remoteConfig()["advertising"].boolValue)
        }
      }
    }
  }
}
